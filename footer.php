<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package abcs
 */

?>

	</div><!-- #content -->

<?php 
  $logo_image_object = get_field('logo', 'option'); 
  $logo_small_object = get_field('logo_small', 'option'); 
  $logo_icon_object = get_field('logo_icon', 'option'); 

  $organization_name = get_field('organization_name', 'option'); 
  $address = get_field('address', 'option'); 
  $city = get_field('city', 'option'); 
  $state = get_field('state', 'option'); 
  $zipcode = get_field('zipcode', 'option'); 
  $phone = get_field('phone', 'option'); 
  $email = get_field('email', 'option'); 
  $social_channels = get_field('social_channels', 'option'); 

  $main_site_name = '';

  // Get ABCS social if none
  switch_to_blog(1);
    if(!$social_channels){
      $social_channels = get_field('social_channels', 'option'); 
    }
    $main_site_name = get_field('organization_name', 'option'); 
  restore_current_blog();
?>

	<footer id="site-footer" class="footer">
		<div class="container">
			<div class="row">
  			<div class="col-sm-4 col-md-4 floater">
  				<h4>Email newsletter</h4>
  				<p>Sign up for our email newsletter to keep up with the latest news from <?php echo $main_site_name; ?>.</p>
  				<p class="fake-placeholder">Email Address</p>
          <!-- Begin Constant Contact Active Forms -->
          <!-- Begin Constant Contact Inline Form Code -->
          <div class="ctct-inline-form" data-form-id="beabd3b5-3bfd-4509-bca4-0afc555998d6"></div>
          <!-- End Constant Contact Inline Form Code -->
          <script> var _ctct_m = "d60a2895b7ff5d0645fc49773bd07039"; </script>
          <script id="signupScript" src="//static.ctctcdn.com/js/signup-form-widget/current/signup-form-widget.min.js" async defer></script>
          <!-- End Constant Contact Active Forms -->
        </div>
  			<div class="col-sm-4 floater">
  				<div class="row-normal">
  		      <div class="col-sm-6 nmb">
  						<?php
  		          $args = array(
  		            'menu' => 'footer-1',
  		            'menu_class' => 'nav',
  		            'menu_id' => 'navigation-footer-1',
  		            'container' => false,
  		            'theme_location' => 'footer-1'
  		          );
  		          wp_nav_menu($args);
  		        ?>
  		      </div>
  		      <div class="col-sm-6">
  						<?php
  		          $args = array(
  		            'menu' => 'footer-2',
  		            'menu_class' => 'nav',
  		            'menu_id' => 'navigation-footer-2',
  		            'container' => false,
  		            'theme_location' => 'footer-2'
  		          );
  		          wp_nav_menu($args);
  		        ?>
  		      </div>
  		    </div>
  		  </div>

  			<div class="col-sm-2 col-md-3 floater">
  				<p class="maxp-220"><?php echo $organization_name; ?>
          <?php if($address): ?>
  					<br/><?php echo $address; ?>
  					<br/><?php echo $city; ?>, <?php echo $state; ?> <?php echo $zipcode; ?>
          <?php endif; ?>
          </p>
  				<p><?php echo $phone; ?>
  					<br/><?php echo $email; ?></p>
  			</div>

  			<div class="col-sm-2 col-md-1 floater">
  				<img src="<?php echo $logo_small_object['sizes']['medium']; ?>" height="60" alt="<?php echo get_bloginfo('name'); ?>" class="logo-icon"/>
  			</div>
      </div>

      <div class="row">
        <div class="col-sm-12 social-links">
          <ul class="social-icons list-inline">
            <?php 
              if($social_channels):
                foreach($social_channels as $channel):
            ?>
              <li><a href="<?php echo $channel['profile_url']; ?>" title="Go to <?php echo $channel['username']; ?> on <?php echo $channel['channel']; ?>" target="_blank" class="brand-buttonhovercolor"><span class="fa fa-<?php echo $channel['channel']; ?>"></span></a></li>
            <?php
                endforeach;
              endif;
            ?>
          </ul>
          <div class="line"></div>
        </div>
        <div class="col-sm-12 abcs-ministry text-center">
          <?php 
            if(get_current_blog_id() !== 1):
              switch_to_blog( 1 );
          ?>
            <p>A Ministry of <a href="<?php echo get_bloginfo('url'); ?>" title="View <?php echo get_bloginfo('name'); ?>" class="text-mdgrey brand-texthovercolor"><?php echo get_bloginfo('name'); ?></a></p>
          <?php 
            restore_current_blog();
            endif;
          ?>
        </div>
      </div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<!-- JSON-LD markup for business. -->
  <script type="application/ld+json">
  {
    "@context" : "http://schema.org",
    "@type" : "LocalBusiness",
    "name" : "<?php echo $organization_name; ?>",
    "location": {
      "@type": "Place"
    },
    "address": {
      "@type": "PostalAddress",
      "streetAddress": "<?php echo $address; ?>",
      "addressLocality": "<?php echo $city; ?>",
      "addressRegion": "<?php echo $state; ?>",
      "postalCode": "<?php echo $zipcode; ?>"
    },
    "description" : "<?php bloginfo( 'description' ); ?>",
    "url" : "<?php the_permalink(); ?>",
    "telephone": "<?php echo $phone; ?>",
    "email": "<?php echo $email; ?>",
    "image": "<?php echo $logo_image_object['sizes']['medium']; ?>"
  }
  </script>


  <?php force_pdf_open_form(); ?>
  
  <?php 
    // Custom Code in Customizer
    // see inc/customizer.php for more.
    // allows users to add code to footer, like a chat widget.
    echo get_theme_mod('custom_footer_code', ''); 
  ?>

<?php wp_footer(); ?>


  <?php 
    // Custom Tracking Pixel
    // Setup on Sept 4, 2024 per Chellsea
    //
  ?>
  <script src="https://js.adsrvr.org/up_loader.1.1.0.js" type="text/javascript"></script>
  <script type="text/javascript">
      ttd_dom_ready( function() {
          if (typeof TTDUniversalPixelApi === 'function') {
              var universalPixelApi = new TTDUniversalPixelApi();
              universalPixelApi.init("gqadkls", ["3yacpr1"], "https://insight.adsrvr.org/track/up");
          }
      });
  </script>

</body>
</html>
