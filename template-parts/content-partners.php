<?php
/**
 * Template part for displaying partner information
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package abcs
 */

?>
<section>
  <article id="post-<?php the_ID(); ?>" <?php post_class('partner-page'); ?>>
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <header class="entry-header">
            <?php
            if ( is_singular() ) :
              the_title( '<h1 class="title">', '</h1>' );
            else :
              the_title( '<h2 class="title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
            endif; 
            ?>
          </header><!-- .entry-header -->
        </div>
      </div>

      <div class="row">
        <div class="col-xs-12 col-sm-6 text-left pull-left">
          <div class="entry-content">
            <?php
              if( get_post_type( get_the_ID() ) == 'partners' ) {
                $name = get_field('name');
                $status = get_field('status');
                $distribution_site = get_field('distribution_site');
                $repeats = get_field('repeats');
                $weeks = get_field('weeks');
                $days = get_field('days');
                $start_time = get_field('start_time');
                $end_time = get_field('end_time');
                $address = get_field('address');
                $zip = get_field('zip');
                $contact_name = get_field('contact_name');
                $public_phone = get_field('public_phone');
                $url = get_field('website_url');
                $latitude = get_field('latitude');
                $longitude = get_field('longitude');
                $icon_object = get_field('icon');
                $icon = $icon_object['sizes']['medium'];
                $clean_url = str_replace("http://", "", $url);
                $clean_url = str_replace("https://", "", $clean_url);
                $clean_url = str_replace("/", "", $clean_url);
              }


              if($status == 'premier') {
                echo '<p class="subtitle">Premier Partner</p>';
              } else {
                echo '<p class="subtitle">Caring Partner</p>';
              }
              echo '<p>' . $address . 
                    '<br/>Tucson, AZ ' . $zip . 
                    '<br/>' . $website_url . '</p>';
              echo '<hr/>';

              if($distribution_site == 1) {
                echo '<h4>Distribution site</h4>';

                if($repeats === 'weekly') {
                  echo '<p>Every';
                }

                if($repeats === 'monthly') {
                  echo '<p>' . $weeks['label'];
                }
                $day_count = 0;

                foreach ($days as $day) {
                  if($day_count > 0 && count($days) != 2){
                    echo ',';
                  } else {
                    echo '';
                  }
                  if(count($days) > 1 && $day_count == count($days) - 1) {
                    echo ' and ';
                  }
                  echo ' ' . $day['label'];

                  $day_count++;
                }

                if($repeats === 'monthly') {
                  echo ' of the month';
                }

                echo '<br/>' . $start_time . '–' . $end_time;

                echo '</p>';
              }


              if(($contact_name && $public_phone) || ($contact_name && $url)) {
                echo '<br/><h4>Contact information</h4>';
                echo '<p>' . $contact_name;
                if($public_phone){
                  echo '<br/>' . $public_phone;
                }
                if($url){
                  echo '<br/><a href="'. $url .'" target="_blank">' . $clean_url . '</a></p>';
                }
              }
            ?>


          </div><!-- .entry-content -->
        </div>

        <div class="col-xs-12 col-sm-6 pull-right">
          <?php $image_med = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium'); ?>   
          <?php $image_lg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large'); ?>     
          <div style="background-image:url('<?php echo $image_med[0]; ?>');" alt="" class="img-aspect-4-3 b-lazy" data-src="<?php echo $image_lg[0]; ?>"></div>
        </div>

      </div>
      <div class="row">
        <div class="col-xs-12">         
          <hr/>
          <p><a href="/get-food" title="View all food locations">< View all food locations</a></p>
        </div>
      </div>

    </div>
  </article><!-- #post-<?php the_ID(); ?> -->
</section>
