<?php
/**
 * Template part for displaying event detail content 
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package abcs
 */
  
  $this_post = $post;

  $display_name = get_field('display_name', $this_post->ID); // Text
  $location_type = get_field('location_type', $this_post->ID); // Radio ('abcs' or 'external')
  $location = get_field('location', $this_post->ID); // Relationship
  $image = get_field('image', $this_post->ID); // Image
  $short_description = get_field('short_description', $this_post->ID); // Textarea
  $description = get_field('description', $this_post->ID); // Wysiwyg Editor
  $repeats = get_field('repeats', $this_post->ID); // Radio Button
  $contact_name = get_field('contact_name', $this_post->ID); // Text
  $phone = get_field('phone', $this_post->ID); // Text
  $email = get_field('email', $this_post->ID); // Text
  $website_url = get_field('website_url', $this_post->ID); // Url

  // If Location is an ABCS site.
  if($location_type != 'external'){
    $thisID = $location[0]->ID; 
    $location_url = get_the_permalink( $thisID );
    $location_name = get_field('ministry_location_name', $thisID); // Text
    $location_ID = get_field('ministry_location_select', $thisID); // Select
    $location_phone = get_field('ministry_location_phone', $thisID); // text
    $location_staff = get_field('ministry_location_staff', $thisID); // Relationship
    $hours = get_field('ministry_location_hours', $thisID); // Repeater

    $main = [];
    switch_to_blog(1);
      $main['display_name'] = get_field('display_name', $location_ID); // Text
      $main['phone'] = get_field('phone', $location_ID); // Text
      $main['address'] = get_field('address', $location_ID); // Text
      $main['city'] = get_field('city', $location_ID); // Text
      $main['state'] = get_field('state', $location_ID); // Select
      $main['zip'] = get_field('zip', $location_ID); // Text
      $main['longitude'] = get_field('longitude', $location_ID); // Text
      $main['latitude'] = get_field('latitude', $location_ID); // Text
      $main['default_days'] = get_field('days', $location_ID); // Repeater
      $main['url'] = get_the_permalink( $location_ID );
    restore_current_blog();

  } else {
    // Custom Location for this event.
    $location_url = '';
    $location_name = get_field('custom_location_name', $this_post->ID); // Text
    $location_map = get_field('custom_location_map', $this_post->ID); // Google Map
    $location_phone = '';
    $location_staff = '';
    $hours = '';
    $main = [
      'address' => get_field('custom_location_address', $this_post->ID), // Text
      'city' => get_field('custom_location_city', $this_post->ID), // Text
      'state' => get_field('custom_location_state', $this_post->ID), // Text
      'zip' => get_field('custom_location_zip', $this_post->ID), // Text
      'longitude' => $location_map['lng'],
      'latitude' => $location_map['lat'],
    ];
  }

  wp_reset_postdata();
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <section class="hero hero-text">
    <div class="container">
      <div class="row">
        <div class="hero-content col-xs-12">
          <h1 class="black"><?php echo $display_name; ?></h1>
        </div>
      </div>
      <div id="bottom-of-hero"></div>
    </div>
  </section>

  <section class="two-columns">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-6 left">

          <?php if($short_description): ?>
            <h2 class="mb2"><?php echo $short_description; ?></h2>
          <?php endif; ?>

          <?php if($repeats === 'weekly'):
            $days_of_the_week = get_field('days_of_the_week', $this_post->ID); // Repeater
            $start_date = get_field('start_date', $this_post->ID); // Date Picker
            $end_date = get_field('end_date', $this_post->ID); // Date Picker
          ?>
            <ul class="list-unstyled list-dates text-sans bb">
              <li>
                <b>From <?php echo date('M j', strtotime($start_date)); ?> to <?php echo date('M j', strtotime($end_date)); ?></b>
              </li>
              <?php 
                $dates = get_field('dates', $this_post->ID);
                foreach($days_of_the_week as $day): 
              ?>
                <li><span class="pull-right"><?php echo $day['start_time']; ?><?php echo ($day['end_time'] ? '–' . $day['end_time'] : ''); ?></span><?php echo date('l', strtotime("Sunday +{$day['day']} days")) . 's'; ?></li>
              <?php endforeach; ?>
            </ul>

          <?php else: ?>
            
            <?php 
              $dates = get_field('dates', $this_post->ID);
              $dates_count = count($dates);
              $multiple_dates = $dates_count > 1;
            ?>
            
            <?php if($multiple_dates): ?>
              <p class='text-sans'><b>Event Dates</b></p>
            <?php endif; ?>

            <ul class="list-unstyled list-dates text-sans <?php if($multiple_dates){ echo 'multiple-dates'; } ?>">
              <?php 
                foreach($dates as $event): 
              ?>
                <li><?php echo date('l, F j, Y', strtotime($event['date'])); ?><br/><?php echo $event['start_time']; ?><?php echo ($event['end_time'] ? '–' . $event['end_time'] : ''); ?></li>
              <?php endforeach; ?>
            </ul>

          <?php endif; ?>

          <p class='text-sans'><b>Location</b>
            <?php if($location_type == 'virtual'): ?>
              <br/>Virtual Event
            <?php else: ?>
              <br/><?php echo $location_name; ?>
              <br/><a href="https://www.google.com/maps/search/?api=1&query=<?php echo $main['latitude']; ?>,<?php echo $main['longitude']; ?>" title="Map Link to <?php echo $location_name; ?>" target="_blank" class="link-normal"> <?php echo $main['address'] . '<br/>' . $main['city'] . ', ' . $main['state'] . ' ' . $main['zip'];  ?></a>
            <?php endif; ?>
          </p>

          <p class="text-sans"><b>Contact</b>
            <?php echo ($contact_name ? '<br/>' . $contact_name : ''); ?>
            <?php echo ($phone ? '<br/>' . $phone : ''); ?>
            <?php echo ($email ? '<br/>' . $email : ''); ?>

            <?php if($website_url): ?>
              <br/><a href="<?php echo $website_url; ?>" title="<?php echo $display_name; ?>" target="_blank">Event website ></a>
            <?php endif; ?>
          </p>
          
          <div class="bt pt">
            <div class="text-sans"><?php echo $description; ?></div>
          </div>

        </div>
        <div class="col-xs-12 col-sm-6 right">
          <div id='map'>
            <?php if($location): ?>
              <location data-longitude="<?php echo $main['longitude']; ?>" data-latitude="<?php echo $main['latitude']; ?>" data-name="<?php echo $location_name; ?>" data-note="" data-url="<?php echo $location_url; ?>" data-address="<?php echo $main['address']; ?>"></location>
            <?php endif; ?>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 mt2">
          <div class="bt pt">
            <p class="text-sans"><a href="<?php echo home_url('/events'); ?>" class="brand-textcolor" title="View all events">< All Events</a></p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php  // loop through the rows of data
    get_template_part( 'template-parts/content', 'page' );
  ?>

</div><!-- #post-<?php the_ID(); ?> -->