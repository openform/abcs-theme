<?php
/**
 * Template part for displaying Ministry Locations
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package abcs
 */

?>

<?php
// $fields = get_field_objects();
// var_dump( $post->ID ); 
  // Ministry Location stuff
  $thisID = get_the_id();
  $location_name = get_field('ministry_location_name', $thisID); // Text
  $location_ID = get_field('ministry_location_select', $thisID); // Select
  $phone = get_field('ministry_location_phone', $thisID); // text
  $location_staff = get_field('ministry_location_staff', $thisID); // Relationship
  $hours = get_field('ministry_location_hours', $thisID); // Repeater
  $hide_hours = get_field('hide_hours', $thisID); // True false.
  $ministry_hours_note = get_field('ministry_location_hours_note', $thisID); // Text

  // Location Building Stuff
  $main = [];
  switch_to_blog(1);
    $main['display_name'] = get_field('display_name', $location_ID); // Text
    $main['phone'] = get_field('phone', $location_ID); // Text
    $main['image'] = get_field('image', $location_ID); // Image
    $main['location_note'] = get_field('location_note', $location_ID); // Text Area
    $main['location_url'] = get_field('location_url', $location_ID); // Url
    $main['address'] = get_field('address', $location_ID); // Text
    $main['city'] = get_field('city', $location_ID); // Text
    $main['state'] = get_field('state', $location_ID); // Select
    $main['zip'] = get_field('zip', $location_ID); // Text
    $main['longitude'] = get_field('longitude', $location_ID); // Text
    $main['latitude'] = get_field('latitude', $location_ID); // Text
    $main['default_days'] = get_field('days', $location_ID); // Repeater
    $main['default_hours_note'] = get_field('hours_note', $location_ID); // Text
  restore_current_blog();


  // Override defaults
  if(!$location_name) {
    if ($main['display_name']) {
      $location_name = $main['display_name'];
    } else {
      $location_name = get_the_title();
    }
  }
  if(!$phone) {
    if ($main['phone']) {
      $phone = $main['phone'];
    } else {
      $phone = false;
    }
  }
  if(!$hours) {
    if($main['default_days']) {
      $hours = $main['default_days'];
    } else {
      $hours = false;
    }
  }
  // override Hours
  if($hide_hours){
    $hours = false; 
  }
  if(!$ministry_hours_note) {
    if($main['default_hours_note']) {
      $ministry_hours_note = $main['default_hours_note'];
    } else {
      $ministry_hours_note = false;
    }
  }
?>
<section>
  <article id="post-<?php the_ID(); ?>" <?php post_class('location-page mlp'); ?>>
    <div class="container">
      <div class="row mb2">
        <div class="col-xs-12 col-sm-6">
          <header class="entry-header mb">
            <h1 class="title"><?php echo $location_name; ?></h1>
          </header><!-- .entry-header -->
          <p class="text-sans"><?php echo $main['address']; ?>
            <br/><?php echo $main['city'] . ', ' . $main['state'] . ' ' . $main['zip']; ?>
            <?php if($phone){ ?>
              <br/><?php echo $phone; ?>
            <?php } ?>
          </p>
          <?php if($hours) { ?>
            <ul class="list-unstyled text-sans">
              <?php foreach($hours as $day): ?>
                <?php 
                  $day_offset = $day['day_of_the_week'] - 1;
                  $dowtext = date('l', strtotime("Sunday +{$day_offset} days"))
                ?>
                <li><span class="day"><?php echo $dowtext; ?></span><span class="hourtime"><?php echo $day['start_time'] . ' – ' . $day['end_time']; ?></span></li>
              <?php endforeach; ?>
            </ul>
          <?php } ?>
          
          <?php if($ministry_hours_note) { ?>
            <p class="text-sans"><?php echo $ministry_hours_note; ?></p>
          <?php } ?>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-6">
              <?php if($main['image'] && in_array('sizes', $main['image'])){ ?>
                <img src="<?php echo $main['image']['sizes']['medium']; ?>" alt="">
              <?php } ?>
            </div>
            <div class="col-sm-6">
              <?php if($main['location_note']) { ?>
                <p class="text-sans"><?php echo $main['location_note']; ?></p>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>

      <div class="row mb3">
        <div class="col-sm-6">
          <div class="bt">
            <h3 class="mt mb">Services</h3>
              <?php
              $services_available_array = array();
              $args = array( 
                'post_type' => 'services', 
                'posts_per_page' => -1,
                'orderby'    => 'title',
                'order'       => 'ASC',
              );
              $loop = new WP_Query( $args );
              if( $loop->have_posts() ):
                echo "<ul class='list-unstyled lined'>";
                while ( $loop->have_posts() ) : $loop->the_post();
                  $service_name = get_field('display_name');
                  $service_url = get_the_permalink();
                  $service_locations = get_field('service_locations');

                  foreach($service_locations as $site){

                    if($site['location_site'] && $site['location_site'][0] == $thisID){
                      $service_note = $site['location_notes'];
                      array_push($services_available_array, array(
                        'service_name' => $service_name,
                        'service_url' => $service_url,
                      ));
                      ?>
                        <li class="text-sans"><span class="service-name"><a href="<?php echo $service_url; ?>" class="brand-textcolor" title="View <?php echo $service_name; ?>" ><?php echo $service_name; ?></a></span><span class="service-note"><?php echo $service_note; ?></span></li>
                      <?php
                      break;
                    }
                  }
                endwhile; 
                echo "</ul>";
              endif;
              ?>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="map">
            <div id='map' class="single-marker"  data-lat="<?php echo $main['latitude']; ?>" data-lng="<?php echo $main['longitude']; ?>" data-zoom="13">
              <location 
                data-longitude="<?php echo $main['longitude']; ?>" 
                data-latitude="<?php echo $main['latitude']; ?>" 
                data-name="<?php echo $location_name; ?>" 
                data-note="<?php echo $main['location_note']; ?>" 
                data-url="<?php echo $main['location_url']; ?>" 
                data-focus="13.5" 
                data-address="<?php echo $main['address']; ?>"></location>
            </div>
          </div>
        </div>
      </div>
      <?php if($location_staff && $location_staff[0]): ?>
        <div class="row mt2">
          <div class="col-sm-12">
            <div class="bt">
              <h3 class="mt">Staff</h3>
              <div class="media-block-row">
                <div class="row">
                  <?php foreach($location_staff as $person): 
                    if ( has_post_thumbnail($person->ID) ) {
                        $large_image = wp_get_attachment_image_src( get_post_thumbnail_id($person->ID), 'large');
                        $large_image = $large_image[0];
                        $medium_image = wp_get_attachment_image_src( get_post_thumbnail_id($person->ID), 'medium');
                        $medium_image = $medium_image[0];
                    } else {
                        $large_image = false;
                        $medium_image = false;
                    }
                    $display_name = get_field('display_name', $person->ID);
                    $role = get_field('role', $person->ID);
                    $email = get_field('email', $person->ID);
                    $staff_phone = get_field('phone', $person->ID);
                    $clean_phone = str_replace(array(' ','_','(',')','–','—'), '', $staff_phone);
                    $bio = get_field('bio', $person->ID);
                  ?>

                    <div class="col-sm-3">
                      <article class="media-block">

                        <?php if($medium_image): ?>
                          <img src="<?php echo $medium_image; ?>" alt="<?php echo $display_name; ?>" class="media-image b-lazy" data-src="<?php echo $large_image; ?>">
                        <?php endif; ?>

                        <header class="media-text">
                          <p class="text-sans"><?php echo $display_name; ?>
                            <br/><?php echo $role; ?>
                            <?php if($staff_phone): ?>
                              <br/><a href="tel:$clean_phone" class="phone-link brand-texthovercolor"><?php echo $staff_phone; ?></a>
                            <?php endif; ?>
                            <?php if($email): ?>
                              <br/><a href="mailto:<?php echo $email; ?>" class="email-link brand-textcolor brand-texthovercolor"><?php echo $email; ?></a>
                            <?php endif; ?>
                          </p>

                          <?php if($bio): ?>
                            <?php if(strlen($bio) > 145): ?>
                              <?php echo substr($bio, 0, 140); ?>...
                            <?php else: ?>
                              <?php echo $bio; ?>
                            <?php endif; ?>
                          <?php endif; ?>
                        </header>
                      </article>
                    </div>
                  <?php endforeach; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>

      <div class="row">
        <div class="col-sm-12">
          <hr/>
          <a href="<?php echo site_url('/locations'); ?>" title="View all locations" class="brand-texthovercolor">< view all locations</a>
        </div>
      </div>
      
    </div>

    <?php
      $logo_image_object = get_field('logo', 'option'); 
      $organization_name = get_field('organization_name', 'option'); 
      
    ?>
    <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "Service",
      "serviceType": "<?php echo get_bloginfo( 'description' ); ?>",
      "provider": {
        "@type": "LocalBusiness",
        "name": "<?php echo $organization_name; ?>",
        "url": "<?php echo get_the_permalink(); ?>",
        "telephone": "<?php echo $phone; ?>",
        "image": "<?php echo $logo_image_object['sizes']['medium']; ?>",
        "address": {
          "@type": "PostalAddress",
          "streetAddress": "<?php echo $main['address']; ?>",
          "addressLocality": "<?php echo $main['city']; ?>",
          "addressRegion": "<?php echo $main['state']; ?>",
          "postalCode": "<?php echo $main['zip']; ?>"
        }
      },
      "areaServed": {
        "@type": "Place",
        "geo": {
          "@type": "GeoCircle",
          "geoMidpoint": {
            "@type": "GeoCoordinates",
            "latitude": "<?php echo $main['latitude']; ?>",
            "longitude": "<?php echo $main['longitude']; ?>"
          },
          "geoRadius": "20"
        }
      },
      "hasOfferCatalog": {
        "@type": "OfferCatalog",
        "name": "Services at <?php echo $location_name; ?>",
        "itemListElement": [
          <?php
            $serv_idx = 0;
            $serv_count = count($services_available_array); 
            foreach($services_available_array as $serv): ?>
              {
                "@type": "Offer",
                "itemOffered": {
                  "@type": "Service",
                  "name": "<?php echo $serv['service_name']; ?>"
                }
              }

          <?php 
              $serv_idx++;
              if($serv_count > 1 && $serv_count !== $serv_idx){
                echo ',';
              }
            endforeach; 
          ?>
        ]
      }
    }
    </script>

  </article><!-- #post-<?php the_ID(); ?> -->
</section>
