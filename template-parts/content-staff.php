<?php
/**
 * Template part for displaying Staff Detail pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package abcs
 */

?>

<?php
  $display_name = get_field('display_name'); // Text
  $role = get_field('role'); // Text
  $staff_phone = get_field('phone'); // Text
  $email = get_field('email'); // Text
  $bio = get_field('bio'); // wysiwyg
  $phone = str_replace(array(' ','_','(',')','–','—'), '', $staff_phone);

  if ( has_post_thumbnail() ) {
    $large_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');
    $large_image = $large_image[0];
    $medium_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium');
    $medium_image = $medium_image[0];
  } else {
    $large_image = false;
    $medium_image = false;
  }
?>

<section>
  <article id="post-<?php the_ID(); ?>" <?php post_class('staff-page'); ?>>
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-8">
          <header class="entry-header">
            <h1 class="title"><?php echo $display_name; ?></h1>
            <?php if($role): ?><p><?php echo $role; ?></p><?php endif; ?>
            <?php if($phone || $email): ?>
              <p><?php if($phone): ?><?php echo $phone; ?><br/><?php endif; ?>
                <?php if($email): ?><?php echo $email; ?><?php endif; ?></p>
            <?php endif; ?>
          </header><!-- .entry-header -->
          <div class="entry-content max-450">
            <?php echo $bio; ?>
          </div>
        </div>
        <div class="col-xs-12 col-sm-4">
          <img src="<?php echo $medium_image; ?>" class="b-lazy" data-src="<?php echo $large_image; ?>" /> 
        </div>
      </div>

    </div>
  </article><!-- #post-<?php the_ID(); ?> -->
</section>
