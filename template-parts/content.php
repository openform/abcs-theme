<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package abcs
 */

?>
<section>
	<article id="post-<?php the_ID(); ?>" <?php post_class( (get_post_type( get_the_ID() ) === 'post' ? 'blog-post' : '') ); ?>>
		<div class="container">
	    <div class="row mb2">
	      <div class="col-xs-12">
					<header class="entry-header">
						<?php
						if ( is_singular() ) :
							the_title( '<h1 class="entry-title">', '</h1>' );
						else :
							the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
						endif;

						if ( 'post' === get_post_type() ) :
							?>
							<div class="entry-meta">
								<span class="posted-on">Posted on <?php echo get_the_date('', get_the_ID()); ?></span>
								<p class="text-sans">Categories: <?php echo get_the_category_list( ', ', '', get_the_ID() ); ?></p>
							</div><!-- .entry-meta -->
						<?php endif; ?>
					</header><!-- .entry-header -->
				</div>
			</div>

			
			<div class="row">
				<div class="col-xs-12 col-sm-12 text-left">
					<div class="entry-content">

						<?php
							if( get_post_type( get_the_ID() ) === 'post' ) {
								the_content( sprintf(
									wp_kses(
										/* translators: %s: Name of current post. Only visible to screen readers */
										__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'caring' ),
										array(
											'span' => array(
												'class' => array(),
											),
										)
									),
									get_the_title()
								) );

								wp_link_pages( array(
									'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'caring' ),
									'after'  => '</div>',
								) );
							}
						?>
					</div><!-- .entry-content -->
				</div>

			</div>

			<div class="row">
				<div class="col-xs-12">
					<hr/>
					<?php the_post_navigation(); ?>
				</div>
			</div>

		</div>
	</article><!-- #post-<?php the_ID(); ?> -->
</section>
