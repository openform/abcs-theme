<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package abcs
 */
  $service_ID = get_the_id();
  $service_name = get_field('display_name');
  $service_excerpt = get_field('excerpt');
  $service_url = get_the_permalink();
  $service_locations = get_field('service_locations');

  $logo_image_object = get_field('logo', 'option'); 
  $organization_name = get_field('organization_name', 'option'); 
  $address = get_field('address', 'option'); 
  $city = get_field('city', 'option'); 
  $state = get_field('state', 'option'); 
  $zipcode = get_field('zipcode', 'option'); 
  $phone = get_field('phone', 'option'); 
  $email = get_field('email', 'option'); 
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <section class="hero hero-text">
    <div class="container">
      <div class="row">
        <div class="hero-content col-xs-12">
          <h1 class="black"><?php echo $service_name; ?></h1>
        </div>
      </div>
      <div id="bottom-of-hero"></div>
    </div>
  </section>
  <section class="lead npt">
    <div class="container">
      <div class="row">
        <div class="lead-content col-xs-12 col-sm-12 col-md-10 col-lg-8">
          <h2><?php echo $service_excerpt; ?></h2>
          <?php if($service_locations && count($service_locations) > 0): ?>
            <p><a href="<?php echo home_url('/locations/?ms='); ?><?php echo $service_ID; ?>" title="View Locations" class="btn">View <?php echo count($service_locations); ?> Location<?php echo (count($service_locations) > 1 ? 's' : ''); ?></a></p> 
          <?php endif; ?>
        </div>
      </div>
    </div>
  </section>

  <?php  // loop through the rows of data
    wp_reset_postdata();
    
    while ( have_rows('section') ) : the_row();

        if( get_row_layout() == 'hero' ):
          get_template_part( 'template-parts/hero' );

        elseif( get_row_layout() == 'lead' ): 
          get_template_part( 'template-parts/lead' );

        elseif( get_row_layout() == 'story_block' ): 
          get_template_part( 'template-parts/story_block' );

        elseif( get_row_layout() == 'media_block_row' ): 
          get_template_part( 'template-parts/media_block' );

        elseif( get_row_layout() == 'fullwidth_image' ): 
          get_template_part( 'template-parts/fullwidth_image' );

        elseif( get_row_layout() == 'half_and_half' ): 
          get_template_part( 'template-parts/half_and_half' );

        elseif( get_row_layout() == 'call_to_action' ): 
          get_template_part( 'template-parts/call_to_action' );

        elseif( get_row_layout() == 'two_columns' ): 
          get_template_part( 'template-parts/two_columns' );

        elseif( get_row_layout() == 'donation_form' ): 
          get_template_part( 'template-parts/donation_form' );

        elseif( get_row_layout() == 'full_width_text' ): 
          get_template_part( 'template-parts/full_width_text' );

        endif;

    endwhile;
  ?>

  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "Service",
      "serviceType": "<?php echo $service_name; ?>",
      "provider": {
        "@type": "LocalBusiness",
        "name": "<?php echo $organization_name; ?>",
        "image": "<?php echo $logo_image_object['sizes']['medium']; ?>",
        "address": {
          "@type": "PostalAddress",
          "streetAddress": "<?php echo $address; ?>",
          "addressLocality": "<?php echo $city; ?>",
          "addressRegion": "<?php echo $state; ?>",
          "postalCode": "<?php echo $zipcode; ?>"
        },
        "url" : "<?php the_permalink(); ?>",
        "telephone": "<?php echo $phone; ?>",
        "email": "<?php echo $email; ?>"
      },
      "areaServed": {
        "@type": "State",
        "name": "<?php echo $state; ?>"
      }
    }
    </script>
</div><!-- #post-<?php the_ID(); ?> -->
