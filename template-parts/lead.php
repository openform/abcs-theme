<?php
/**
 * Template part for displaying Lead Text.
 *
 * @package abcs
 */

?>

<?php
  $lead_text  = get_sub_field('lead_text'); // text
  $link_text  = get_sub_field('link_text'); // text
  $link_type  = get_sub_field('link_type'); // Radio
  $link_url  = get_sub_field('link_url'); // URL
  $site_page_link  = get_sub_field('site_page_link'); // Page Link
  $allsiteslinks_select = get_sub_field('allsiteslinks_select'); // select
  $link_target  = get_sub_field('link_target'); // URL
  $top_margin  = get_sub_field('top_margin'); // radio
  $background_color  = get_sub_field('background_color'); // radio

  $section_class = (isset($top_margin) && $top_margin == 'none') ? 'npt' : '';
  $section_class .= ($background_color) ? ' ' . $background_color : '';
  
  $page_link = '';
  if($link_type === 'url'){
    $page_link = $link_url;
  } 
  elseif($link_type === 'page'){
    $page_link = $site_page_link;
  } 
  elseif($link_type === 'multisite'){
    $page_link = $allsiteslinks_select;
  }

?>


<section id="lead" class="lead <?php echo $section_class; ?>">
  <div class="container">
    <div class="row">
      <div class="lead-content col-xs-12 col-sm-12 col-md-10 col-lg-8">
        <h2><?php echo $lead_text; ?></h2>
        <?php if($link_text && $page_link !== ''): ?>
          <p><a href="<?php echo $page_link; ?>" title="<?php echo $link_text; ?>" target="<?php echo $link_target; ?>" class="btn"><?php echo $link_text; ?></a></p>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>