<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package abcs
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('bb pb search-item'); ?>>
	<header class="entry-header pt2 pb">
		<?php the_title( sprintf( '<a href="%s" rel="bookmark"><h3 class="entry-title">', esc_url( get_permalink() ) ), '</h3></a>' ); ?>

		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<span class="posted-on">Posted on <?php echo get_the_date('', get_the_ID()); ?></span>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php caring_post_thumbnail(); ?>

	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->

</article><!-- #post-<?php the_ID(); ?> -->
