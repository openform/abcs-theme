<?php
/**
 * Template part for displaying Hero Images.
 *
 * @package abcs
 */

?>

<?php
  $background_image_object  = get_sub_field('background_image'); // image
  $fade  = get_sub_field('fade'); // radio
?>


<section id="fwimg" class="fullwidth-image <?php echo $fade; ?>">
  <div class="fullwidth-image-bg dark b-lazy" style='background-image: url("<?php echo $background_image_object['sizes']['large']; ?>");' data-src="<?php echo $background_image_object['sizes']['huge']; ?>"></div>
</section>