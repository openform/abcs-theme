<?php
/**
 * The template for displaying Blog posts archive section
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package abcs
 */
?>
<?php 
  $num_posts = get_sub_field('num_posts'); // selet
  if(!$num_posts > 2){
    $num_posts = 12;
  }
?>

  <section class="archive media-block-row ">
    <div class="container">
      <div class="row autoclear">

          <?php 
            $args = array( 
              'post_type' => 'post', 
              'posts_per_page' => $num_posts,
              'orderby'    => 'date',
              'order'       => 'DESC',
            );
            $loop = new WP_Query( $args );
            if( $loop->have_posts() ):
              while ( $loop->have_posts() ) : $loop->the_post(); ?>
              <?php
                $thisID = get_the_ID();

                if ( has_post_thumbnail() ) {
                  $large_image = wp_get_attachment_image_src( get_post_thumbnail_id($thisID), 'large');
                  $large_image = $large_image[0];
                  $medium_image = wp_get_attachment_image_src( get_post_thumbnail_id($thisID), 'medium');
                  $medium_image = $medium_image[0];
                } else {
                  $large_image = false;
                  $medium_image = false;
                }
              ?>
              <div class="col-xs-12 col-sm-4 col-md-4">
                <article class="media-block">
                  <a href="<?php the_permalink($thisID); ?>" title="<?php echo get_the_title($thisID); ?>" >
                    <div class="b-lazy img-aspect-4-3" style='background-image: url("<?php echo $medium_image; ?>"); ' data-src="<?php echo $large_image; ?>"></div>
                  </a>
                  <header class="media-text">
                    <h1><?php echo get_the_title($thisID); ?></h1>
                    <p><?php echo get_the_excerpt($thisID); ?></p>
                    <a href="<?php the_permalink($thisID); ?>" title="<?php echo get_the_title($thisID); ?>" class="link-primary brand-bordercolor brand-texthovercolor">Read now</a>
                  </header>
                </article>
              </div>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
          <?php endif; ?>

      </div>
    </div>
  </section>
