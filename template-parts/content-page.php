<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package abcs
 */

?>

	<?php  // loop through the rows of data
    while ( have_rows('section') ) : the_row();

        if( get_row_layout() == 'hero' ):
          get_template_part( 'template-parts/hero' );

        elseif( get_row_layout() == 'lead' ): 
          get_template_part( 'template-parts/lead' );

        elseif( get_row_layout() == 'story_block' ): 
          get_template_part( 'template-parts/story_block' );

        elseif( get_row_layout() == 'media_block_row' ): 
          get_template_part( 'template-parts/media_block' );

        elseif( get_row_layout() == 'fullwidth_image' ): 
          get_template_part( 'template-parts/fullwidth_image' );

        elseif( get_row_layout() == 'half_and_half' ): 
          get_template_part( 'template-parts/half_and_half' );

        elseif( get_row_layout() == 'call_to_action' ): 
          get_template_part( 'template-parts/call_to_action' );

        elseif( get_row_layout() == 'two_columns' ): 
          get_template_part( 'template-parts/two_columns' );

        elseif( get_row_layout() == 'three_columns' ): 
          get_template_part( 'template-parts/three_columns' );

        elseif( get_row_layout() == 'donation_form' ): 
          get_template_part( 'template-parts/donation_form' );

        elseif( get_row_layout() == 'full_width_text' ): 
          get_template_part( 'template-parts/full_width_text' );

        elseif( get_row_layout() == 'seperator' ): 
          get_template_part( 'template-parts/seperator' );

        elseif( get_row_layout() == 'blog_archive' ): 
          get_template_part( 'template-parts/blog-archive' );

        elseif( get_row_layout() == 'locations_map' ):
          if(get_current_blog_id() === 1): 
            get_template_part( 'template-parts/_locations-map-abcs' );
          else:
            get_template_part( 'template-parts/locations-map' );
          endif;

        elseif( get_row_layout() == 'events_calendar' ): 
          get_template_part( 'template-parts/events_calendar' );
        endif;

    endwhile;
  ?>
