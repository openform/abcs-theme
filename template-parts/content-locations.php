<?php
  
/**
 * Template part for displaying an ABCS Location
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package abcs
 */

?>

<?php
  $location_ID = get_the_id();

  require_once(get_template_directory() . '/template-parts/_location_models.php');
  $serv = new Services;
  $building_services = $serv->all_services_by_building();

  // Location Building Stuff
  $location = [];
  switch_to_blog(1);
    $location['display_name'] = get_field('display_name', $location_ID); // Text
    $location['phone'] = get_field('phone', $location_ID); // Text
    $location['image'] = get_field('image', $location_ID); // Image
    $location['location_note'] = get_field('location_note', $location_ID); // Text Area
    $location['location_url'] = get_field('location_url', $location_ID); // Url
    $location['address'] = get_field('address', $location_ID); // Text
    $location['city'] = get_field('city', $location_ID); // Text
    $location['state'] = get_field('state', $location_ID); // Select
    $location['zip'] = get_field('zip', $location_ID); // Text
    $location['longitude'] = get_field('longitude', $location_ID); // Text
    $location['latitude'] = get_field('latitude', $location_ID); // Text
    $location['default_days'] = get_field('days', $location_ID); // Repeater
    $location['default_hours_note'] = get_field('hours_note', $location_ID); // Text
  restore_current_blog();

  $location['ministries'] = $building_services[$location_ID];
  $services_available_array = array();
?>
<section>
  <article id="post-<?php the_ID(); ?>" <?php post_class('location-page'); ?>>
    <div class="container">
      <div class="row mb2">
        <div class="col-xs-12 col-sm-6">
          <header class="entry-header mb">
            <h1 class="title"><?php echo $location['display_name']; ?></h1>
          </header><!-- .entry-header -->
          <p class="text-sans"><?php echo $location['address']; ?>
            <br/><?php echo $location['city'] . ', ' . $location['state'] . ' ' . $location['zip']; ?>
            <?php if($location['phone']){ ?>
              <br/><?php echo $location['phone']; ?>
            <?php } ?>
          </p>
          <?php if($location['default_hours_note']) { ?>
            <p><?php echo $location['default_hours_note']; ?></p>
          <?php } ?>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-6">
              <?php if(isset($location['image']['sizes'])){ ?>
                <img src="<?php echo $location['image']['sizes']['medium']; ?>" alt="">
              <?php } ?>
            </div>
            <div class="col-sm-6">
              <?php if($location['location_note']) { ?>
                <p class="text-sans"><?php echo $location['location_note']; ?></p>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>

      <div class="row mb3">
        <div class="col-sm-6">
          <div class="bt">
            <h3 class="mt mb">Ministry Services</h3>
              <?php if( isset($location['ministries']) && count($location['ministries']) > 0 ): ?>
                <ul class='list-unstyled lined'>
                  <?php foreach($location['ministries'] as $min_id => $min): ?>
                    <?php
                      // let each ministry link to their ministry location page instead of hte ministry home page. 
                      $min_url = $min['url'];
                      // The ministry can't be ABCS, and must have a location page url set.
                      if($min['location_url'] && $min['ministry_id'] !== "1"){
                        $min_url = $min['location_url'];
                      }
                    ?>
                    <li class="text-sans" min-id="<?=$min['ministry_id']?>" min-url="<?=$min['url']?>" min-loc-url="<?=$min['location_url']?>">
                      <span class="ministry-name"><a href="<?php echo $min_url; ?>" class="brand-textcolor" title="View <?php echo $min['ministry_name']; ?>" ><?php echo $min['ministry_name']; ?></a></span>
                      <br/><span class="ministry-services">
                        <?php foreach($min['services'] as $s): ?>
                          <a href="<?php echo $s['url']; ?>" class="brand-texthovercolor small" title="<?php echo $s['name']; ?>"><?php echo $s['name']; ?></a>, 

                          <?php 
                            // build array for json
                            array_push($services_available_array, array('service_name', $s['name']));
                          ?>

                        <?php endforeach; ?>
                      </span>
                    </li>
                  <?php endforeach; ?>
                </ul>
              <?php endif; ?>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="map">
            <div id='map' class="single-marker" data-lat="<?php echo $location['latitude']; ?>" data-lng="<?php echo $location['longitude']; ?>" data-zoom="13">
              <location 
                data-longitude="<?php echo $location['longitude']; ?>" 
                data-latitude="<?php echo $location['latitude']; ?>" 
                data-name="<?php echo $location['display_name']; ?>" 
                data-note="<?php echo $location['location_note']; ?>" 
                data-url="<?php echo $location['url']; ?>" 
                data-focus="13.5" 
                data-address="<?php echo $location['address']; ?>"></location>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <hr/>
          <a href="<?php echo site_url('/locations'); ?>" title="View all locations" class="brand-texthovercolor">< view all locations</a>
        </div>
      </div>
      
    </div>

    <?php
      $logo_image_object = get_field('logo', 'option'); 
      $organization_name = get_field('organization_name', 'option'); 
      
    ?>
    <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "Service",
      "serviceType": "<?php echo get_bloginfo( 'description' ); ?>",
      "provider": {
        "@type": "LocalBusiness",
        "name": "<?php echo $organization_name; ?>",
        "url": "<?php echo get_the_permalink(); ?>",
        "telephone": "<?php echo $location['phone']; ?>",
        "image": "<?php echo $logo_image_object['sizes']['medium']; ?>",
        "address": {
          "@type": "PostalAddress",
          "streetAddress": "<?php echo $location['address']; ?>",
          "addressLocality": "<?php echo $location['city']; ?>",
          "addressRegion": "<?php echo $location['state']; ?>",
          "postalCode": "<?php echo $location['zip']; ?>"
        }
      },
      "areaServed": {
        "@type": "Place",
        "geo": {
          "@type": "GeoCircle",
          "geoMidpoint": {
            "@type": "GeoCoordinates",
            "latitude": "<?php echo $location['latitude']; ?>",
            "longitude": "<?php echo $location['longitude']; ?>"
          },
          "geoRadius": "20"
        }
      },
      "hasOfferCatalog": {
        "@type": "OfferCatalog",
        "name": "Services at <?php echo $location['display_name']; ?>",
        "itemListElement": [
          <?php
            $serv_idx = 0;
            $serv_count = count($services_available_array); 
            foreach($services_available_array as $serv): ?>
              {
                "@type": "Offer",
                "itemOffered": {
                  "@type": "Service",
                  "name": "<?php echo $serv['service_name']; ?>"
                }
              }

          <?php 
              $serv_idx++;
              if($serv_count > 1 && $serv_count !== $serv_idx){
                echo ',';
              }
            endforeach; 
          ?>
        ]
      }
    }
    </script>

  </article><!-- #post-<?php the_ID(); ?> -->
</section>

