<?php
/**
 * Template part for displaying Locations Map
 *
 * @package abcs
 */

?>

<?php
    
  $services_array = array();
  $services_args = array( 
    'post_type' => 'services', 
    'posts_per_page' => -1,
    'orderby'    => 'title',
    'order'       => 'ASC',
  );
  $services_loop = new WP_Query( $services_args );
  if( $services_loop->have_posts() ):
    while ( $services_loop->have_posts() ) : $services_loop->the_post();
      $thisID = get_the_id();
      $service_url = get_the_permalink( $thisID );      
      $service_name = get_field('display_name');
      $service_locations = get_field('service_locations');

      $new_service = array(
        'name' => $service_name,
        'url' => $service_url,
        'id' => $thisID,
        'locations' => $service_locations
      );

      // locations = array('location_site','location_notes');


      array_push($services_array, $new_service);
    endwhile;
  endif;

  // Order the services alpha
  array_multisort( array_map('strtolower', array_column($services_array, 'name')), SORT_ASC, $services_array);

  $location_array = array();
  $args = array( 
    'post_type' => 'ministrylocations', 
    'posts_per_page' => -1,
    'orderby'    => 'title',
    'order'       => 'ASC',
  );
  $loop = new WP_Query( $args );
  if( $loop->have_posts() ):
    while ( $loop->have_posts() ) : $loop->the_post();
      $thisID = get_the_id();
      $location_url = get_the_permalink( $thisID );
      $location_name = get_field('ministry_location_name', $thisID); // Text
      $location_ID = get_field('ministry_location_select', $thisID); // Select
      $phone = get_field('ministry_location_phone', $thisID); // text
      $location_staff = get_field('ministry_location_staff', $thisID); // Relationship
      $hours = get_field('ministry_location_hours', $thisID); // Repeater
      $hide_hours = get_field('hide_hours', $thisID); // True false.

      $main = [];
      switch_to_blog(1);
        $main['display_name'] = get_field('display_name', $location_ID); // Text
        $main['phone'] = get_field('phone', $location_ID); // Text
        $main['address'] = get_field('address', $location_ID); // Text
        $main['city'] = get_field('city', $location_ID); // Text
        $main['state'] = get_field('state', $location_ID); // Select
        $main['zip'] = get_field('zip', $location_ID); // Text
        $main['longitude'] = get_field('longitude', $location_ID); // Text
        $main['latitude'] = get_field('latitude', $location_ID); // Text
        $main['default_days'] = get_field('days', $location_ID); // Repeater
        $main['url'] = get_the_permalink( $location_ID );
      restore_current_blog();


      // Override defaults
      if(!$location_name) {
        if ($main['display_name']) {
          $location_name = $main['display_name'];
        } else {
          $location_name = get_the_title();
        }
      }
      if(!$phone) {
        if ($main['phone']) {
          $phone = $main['phone'];
        } else {
          $phone = false;
        }
      }
      if(!$hours) {
        if($main['default_days']) {
          $hours = $main['default_days'];
        } else {
          $hours = false;
        }
      }

      if($hours) { 
        $hours_html = '<table class="table table-unstyled table-align-right text-sans">';
        foreach($hours as $day): 
          $day_offset = $day['day_of_the_week'] - 1;
          $dowtext = substr(date('D', strtotime("Sunday +{$day_offset} days")), 0, 1);
          $start = str_replace([':00',' '], '', $day['start_time']);
          $end = str_replace([':00',' '], '', $day['end_time']);
          $hours_html .= '<tr><td class="day">' .$dowtext. '</td><td class="hourtime">' .$start. '–' .$end. '</td></tr>';
        endforeach;
        $hours_html .= '</table>';
      } else {
        $hours_html = '';
      }

      // Override Hours showing for ministry site.
      if($hide_hours){
        $hours = false;
        $hours_html = '';
      }

      $loc_serv_array = array();

      foreach ($services_array as $service) {
        $service_id = $service['id'];
        // echo '<br/>service_id = ' . $service_id; 
        foreach ($service['locations'] as $location) {
          // echo '<br/>service_site = ' . $location['location_site'][0];
          // echo '<br/>this_location = ' . $thisID;
          // echo '<br/>this_building = ' . $location_ID;
          if($location['location_site'][0] == $thisID && !in_array($service_id, $loc_serv_array)) {
            array_push($loc_serv_array, $service_id);
          }
        }
      }

      $new_location = array(
        'name' => $location_name,
        'address' => $main['address'],
        'city' => $main['city'],
        'state' => $main['state'],
        'zip' => $main['zip'],
        'phone' => $phone,
        'hours_html' => $hours_html,
        'url' => $location_url,
        'longitude' => $main['longitude'],
        'latitude' => $main['latitude'],
        'ministry_site_id' => $thisID,
        'services_id_array' => $loc_serv_array
      );

      array_push($location_array, $new_location);
    endwhile;
  endif; 

?>


<section id="locations" class="locations-map mb">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-6 pull-right">
        <div id="map" class="filter-map">

          <?php foreach($location_array as $f): ?>
            <location 
              data-longitude="<?php echo $f['longitude']; ?>" 
              data-latitude="<?php echo $f['latitude']; ?>" 
              data-name="<?php echo $f['name']; ?>" 
              data-note="" 
              data-url="<?php echo $f['url']; ?>" 
              data-address="<?php echo $f['address']; ?>" 
              data-services="<?php foreach($f['services_id_array'] as $g){echo ' ms-' . $g;} ?>" class="location active <?php foreach($f['services_id_array'] as $g){echo ' ms-' . $g;} ?>"></location>
          <?php endforeach; ?>

        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
        <form class="form-inline">
          <div class="form-group">
            <select id="serviceFilter" class="form-control">
                <option value="all">All Services</option>
              <?php foreach($services_array as $service): ?>
                <option value="<?php echo $service['id']; ?>"><?php echo $service['name']; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <div class="form-group">
            <select id="geoFilter" class="form-control select-filter">
                <option value="all">All Distances</option>
                <option value="5">< 5 miles</option>
                <option value="10">< 10 miles</option>
                <option value="25">< 25 miles</option>
                <option value="100">< 100 miles</option>
            </select>
          </div>
        </form>
        <?php if(count($location_array) > 0): ?>
          <ul class="list-unstyled list-locations bt pt">

            <?php foreach($location_array as $l): ?>
              <li class="<?php foreach($l['services_id_array'] as $s){echo ' ms-' . $s;} ?> calc-distance" data-longitude="<?php echo $l['longitude']; ?>"  data-latitude="<?php echo $l['latitude']; ?>">
                <a class="text-sans brand-texthovercolor" href="<?php echo $l['url']; ?>" title="View <?php echo $l['name']; ?>" >
                  <div class="location-left">
                    <h4><?php echo $l['name']; ?></h4>
                    <p class="text-sans"><?php echo $l['address']; ?>
                      <br/><?php echo $l['city'] . ', ' . $l['state'] . ' ' . $l['zip']; ?>
                      <br/><?php echo $l['phone']; ?>
                    </p>
                  </div>
                  <div class="location-right">
                    <?php echo $l['hours_html']; ?>
                  </div>
                </a>
              </li>
            <?php endforeach; ?>

          </ul>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>