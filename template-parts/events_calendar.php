<?php
/**
 * Template part for displaying Events List Map
 *
 * @package abcs
 */

?>

<?php
  $events_array = array();
  $events_list_array = array();
  $cal_array = array();
  $args = array( 
    'post_type' => 'events', 
    'posts_per_page' => -1,
    'orderby'    => 'title',
    'order'       => 'ASC',
  );
  $loop = new WP_Query( $args );
  if( $loop->have_posts() ):
    while ( $loop->have_posts() ) : $loop->the_post();
      $this_post = $post;
      $display_name = get_field('display_name', $this_post->ID); // Text
      $location_type = get_field('location_type', $this_post->ID); // Radio ('abcs' or 'external')
      $location = get_field('location', $this_post->ID); // Relationship
      $image = get_field('image', $this_post->ID); // Image
      $short_description = get_field('short_description', $this_post->ID); // textarea
      $description = get_field('description', $this_post->ID); // Wysiwyg Editor
      $repeats = get_field('repeats', $this_post->ID); // Radio Button
      $contact_name = get_field('contact_name', $this_post->ID); // Text
      $email = get_field('email', $this_post->ID); // Text
      $website_url = get_field('website_url', $this_post->ID); // Url

      // If Location is external, not an ABCS site.
      if($location_type != 'external'){
        if($location && $location[0]):
          $thisID = $location[0]->ID; 
          $location_url = get_the_permalink( $thisID );
          $location_name = get_field('ministry_location_name', $thisID); // Text
          $location_ID = get_field('ministry_location_select', $thisID); // Select
          $location_staff = get_field('ministry_location_staff', $thisID); // Relationship
          $hours = get_field('ministry_location_hours', $thisID); // Repeater

          $main = [];
          switch_to_blog(1);
            $main['display_name'] = get_field('display_name', $location_ID); // Text
            $main['address'] = get_field('address', $location_ID); // Text
            $main['city'] = get_field('city', $location_ID); // Text
            $main['state'] = get_field('state', $location_ID); // Select
            $main['zip'] = get_field('zip', $location_ID); // Text
            $main['longitude'] = get_field('longitude', $location_ID); // Text
            $main['latitude'] = get_field('latitude', $location_ID); // Text
            $main['default_days'] = get_field('days', $location_ID); // Repeater
            $main['url'] = get_the_permalink( $location_ID );
          restore_current_blog();
        endif;
      } else {
        // Custom Location for this event.
        $location_url = '';
        $location_name = get_field('custom_location_name', $this_post->ID); // Text
        $location_map = get_field('custom_location_map', $this_post->ID); // Google Map
        $location_staff = '';
        $hours = '';
        $main = [
          'address' => get_field('custom_location_address', $this_post->ID), // Text
          'city' => get_field('custom_location_city', $this_post->ID), // Text
          'state' => get_field('custom_location_state', $this_post->ID), // Text
          'zip' => get_field('custom_location_zip', $this_post->ID), // Text
          'longitude' => $location_map['lng'],
          'latitude' => $location_map['lat'],
        ];
      }

      // echo $display_name;
      // var_dump($location);
      // echo '<hr/>';

      // Override defaults
      if(!$location_name) {
        if ($main['display_name']) {
          $location_name = $main['display_name'];
        } else {
          $location_name = get_the_title();
        }
      }

      if($repeats === 'weekly'):
        $days_of_the_week = get_field('days_of_the_week', $this_post->ID); // Repeater
        $start_date = get_field('start_date', $this_post->ID); // Date Picker
        $end_date = get_field('end_date', $this_post->ID); // Date Picker
        $days_display = '';
        $added_to_list = false;

        // Check that weekly even actually days
        if($days_of_the_week == null){
          continue;
        }
        // Check that we are not after this event has ended.
        if($end_date && strtotime($end_date) < strtotime('today')){
          continue;
        }

        $days_count = 0;
        foreach ($days_of_the_week as $day) {
          if($days_count > 0) {
            $days_display .= '<br/>';
          }
          $days_display .= '<b>' . date('l', strtotime("Sunday +{$day['day']} days")) . 's</b><br/>' . $day['start_time'];
          if($day['end_time']) {
            $days_display .= ' – ' . $day['end_time'];
          }
          $days_count++;
        }

        // add start date if is upcoming
        $begin_end_display = 'From ' . date('M j', strtotime($start_date)) . ' to ' . date('M j', strtotime($end_date));

        $new_event = array(
          'type' => $location_type,
          'event_name' => $display_name,
          'location_name' => $location_name,
          'address' => $main['address'],
          'city' => $main['city'],
          'state' => $main['state'],
          'zip' => $main['zip'],
          'date' => $event['date'], 
          'date_display' => $days_display,
          'begin_end_display' => $begin_end_display,
          'repeats' => 'weekly',
          'start_time' => null, 
          'end_time' => null, 
          'short_description' => $short_description, 
          'description' => $description, 
          'url' => get_the_permalink($this_post->ID)
        );
        array_push($events_array, $new_event);


        // now build the cal array
        $day_counter = 0;
        $days_integers = array();
        $days_times = array();
        foreach($days_of_the_week as $day){
          array_push($days_integers, $day['day']);
          if(!isset($days_times[$day['day']])) {
            $days_times[$day['day']]['start'] = $day['start_time'];
            $days_times[$day['day']]['end'] = $day['end_time'];
          }
        }

        while ( strtotime("+".$day_counter." days", strtotime($start_date)) <= strtotime($end_date)) {
          # code...
          $loop_day = date('w', strtotime("+".$day_counter." days", strtotime($start_date)));
          if(in_array($loop_day, $days_integers)){
            $loop_date = date('Ymd', strtotime("+".$day_counter." days", strtotime($start_date)));
            // need to find the start time for this day. 
            $new_event = array(
              'type' => $location_type,
              'event_name' => $display_name,
              'location_name' => $location_name,
              'address' => $main['address'],
              'city' => $main['city'],
              'state' => $main['state'],
              'zip' => $main['zip'],
              'date' => $loop_date, 
              'repeats' => 'weekly',
              'start_time' => $days_times[$loop_day]['start'], 
              'end_time' => null, 
              'date_display' => $days_display,
              'begin_end_display' => $begin_end_display,
              'short_description' => $short_description, 
              'description' => $description, 
              'url' => get_the_permalink($this_post->ID),
              'image' => $image
            );
            array_push($cal_array, $new_event);


            // now build a new event list item if the repeating date is today or later, and only add one.
            // check if today or later, and we haven't already added this event.
            if($added_to_list === false && $loop_date && strtotime($loop_date) >= strtotime('today')){
              // check if array key exists, if not add it, if so, push to it. 
              if(!array_key_exists($loop_date, $events_list_array) ) {
                  $events_list_array[$loop_date] = array($new_event);
              } else {
                  array_push($events_list_array[$loop_date], $new_event);
              }
              $added_to_list = true;
            } 
          }

          $day_counter++;
        }
        // find the day of the year as YYYYMMDD
        // starting that date, 
        //   add the day counter, beginning at 0, as yyyymmdd again
        //   make sure we're not > the enddate as yyyymmdd
        //   check to see if that date matches a repeating day 
        //   if match, add to array
        //   increment the day counter
        
      else: 
        // single events
        $dates = get_field('dates', $this_post->ID);
        if(!$dates) continue;
        foreach($dates as $event):

          // Check that we are not after this event has ended.
          if(strtotime($event['date']) < strtotime('today')){
            continue;
          }
          $event_date = date('l, F j, Y', strtotime($event['date']));
          $start_time = $event['start_time'];
          $end_time = $event['end_time'];
          $date_display = '<span class="datediplay"><b>' . date('F j', strtotime($event_date)) . '</b><br/>' . date('l', strtotime($event_date)) . '</span>'; 

          $new_event = array(
            'type' => $location_type,
            'event_name' => $display_name,
            'location_name' => $location_name,
            'address' => $main['address'],
            'city' => $main['city'],
            'state' => $main['state'],
            'zip' => $main['zip'],
            'date' => $event['date'], 
            'date_display' => $date_display,
            'begin_end_display' => '',
            'repeats' => $repeats,
            'start_time' => $start_time, 
            'end_time' => $end_time, 
            'short_description' => $short_description,
            'description' => $description, 
            'url' => get_the_permalink($this_post->ID),
            'image' => $image
          );
          array_push($events_array, $new_event);
          array_push($cal_array, $new_event);

          // now build a new event list item if the repeating date is today or later, and only add one.
          // check if array key exists, if not add it, if so, push to it. 
          if(!array_key_exists($event['date'], $events_list_array) ) {
              $events_list_array[$event['date']] = array($new_event);
          } else {
              array_push($events_list_array[$event['date']], $new_event);
          }
        endforeach;
      endif;

    endwhile;
  endif;

  // sort the events array by chronological

?>


<?php
  // Do we have any events??
  $showList = '';
  $showCal = 'active';
  
  if(count($events_list_array) > 0){
    $showList = 'active';
    $showCal = '';
  }
?>
<section id="events" class="events-calendar mb">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="<?php echo $showList; ?> brand-active-textcolor"><a href="#listview" class="brand-texthovercolor" aria-controls="listview" role="tab" data-toggle="tab"><span class="fa fa-list"></span> List View</a></li>
          <li role="presentation" class="<?php echo $showCal; ?> brand-active-textcolor"><a href="#calendarview" class="brand-texthovercolor" aria-controls="calendarview" role="tab" data-toggle="tab" id="showcalendarview"><span class="fa fa-calendar"></span> Calendar View</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content mt">
          <div role="tabpanel" class="tab-pane <?php echo $showList; ?>" id="listview">
            <?php 
              ksort($events_list_array);

              foreach($events_list_array as $date_list): 
                foreach($date_list as $e):?>
                  <article class="bt pt pb">
                    <div class="row">
                      <div class="col-sm-3">
                        <h1 class="nmb">
                          <a href="<?php echo $e['url']; ?>" title="<?php echo $e['event_name']; ?>"><?php echo $e['event_name']; ?></a>
                        </h1>
                        <p class="text-sans"><?php echo $e['begin_end_display']; ?></p>
                      </div>
                      <div class="col-sm-3">
                        <p class="text-sans">
                          <?php echo $e['date_display']; ?>
                          <?php if($e['repeats'] !== 'weekly'): ?>
                            <?php echo ($e['start_time'] ? '<br/>' . $e['start_time'] : ''); ?>
                            <?php echo ($e['end_time'] ? '– ' . $e['end_time'] : ''); ?>  
                          <?php endif; ?>
                        </p>
                      </div>
                      <div class="col-sm-3">
                        <?php if($e['type'] == 'virtual'): ?>
                          <p class="text-sans">Virtual Event</p>
                        <?php else: ?>
                          <p class="text-sans"><?php echo $e['address']; ?><br/><?php echo $e['city']; ?>, <?php echo $e['state']; ?> <?php echo $e['zip']; ?></p>
                        <?php endif; ?>
                      </div>
                      <div class="col-sm-3">
                        <?php
                          $this_excerpt = (strlen(strip_tags($e['description'])) > 105 ? substr(strip_tags($e['description']), 0 , 90) . '...' : strip_tags($e['description']));
                          if($e['short_description']){
                            $this_excerpt = $e['short_description'];
                          }
                        ?>
                        <div class="text-sans"><p><a href="<?php echo $e['url']; ?>" title="' . $e['event_name'] .'" class="link-simple"><?php echo $this_excerpt; ?></a></p></div>
                      </div>
                    </div>
                  </article>
              <?php endforeach; ?>
            <?php endforeach; ?>
          </div>
          <div role="tabpanel" class="tab-pane <?php echo $showCal; ?>" id="calendarview">
            <div class="row">
              <div class="col-sm-12">
                <div id="calendar" class="calendarviewer bt pt"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<script type="text/javascript">
  let eventData = <?php echo json_encode( $cal_array ); ?>;

  jQuery(document).ready(()=>{
    jQuery('#calendar').fullCalendar({
      events: eventData.map(function(event){
        let dt = moment(event.date + ' ' + event.start_time, 'YYYYMMDD HH:ii aa');
        return {
          title: event.event_name,
          start: dt.format('YYYY-MM-DDTHH:SS'),
          url: event.url
        }
      }),
      eventColor: '<?php echo get_field('primary_color', 'option'); ?>',
      eventTextColor: 'white',
      displayEventTIme: true
    });

    jQuery('#showcalendarview').on('shown.bs.tab', function (e) {
      // e.target // newly activated tab
      // e.relatedTarget // previous active tab
      jQuery('#calendar').fullCalendar('render');
    });

  });
</script>


<!-- JSON-LD markup for this event. -->
<?php foreach($cal_array as $ev): ?>
  <script type="application/ld+json">
  {
    "@context" : "http://schema.org",
    "@type" : "Event",
    "name" : "<?php echo $ev['event_name']; ?>",
    "image": "<?php echo $ev['image']['sizes']['large']; ?>",
    "location": {
      "@type": "Place",
      "name": "<?php echo $ev['location_name']; ?>",
      "address": {
        "@type": "PostalAddress",
        "streetAddress": "<?php echo $ev['address']; ?>",
        "addressLocality": "<?php echo $ev['city']; ?>",
        "addressRegion": "<?php echo $ev['state']; ?>",
        "postalCode": "<?php echo $ev['zip']; ?>"
      },
      "url": "<?php echo $ev['url']; ?>"
    },
    "startDate" : "<?php echo date('Y-m-d', strtotime($ev['date'])); ?>T<?php echo date('H:i:s', strtotime($ev['start_time'])); ?>",
    <?php if($end_time): ?>
      "endDate" : "<?php echo date('Y-m-d', strtotime($ev['date'])); ?>T<?php echo date('H:i:s', strtotime($ev['end_time'])); ?>",
    <?php endif; ?>
    "description" : "<?php echo $ev['description']; ?>",
    
    "url" : "<?php echo $ev['url']; ?>"

  }
  </script>
<?php endforeach; ?>