<?php
/**
 * Template part for displaying Donation form
 *
 * @package abcs
 */

?>

<?php
  $title  = get_sub_field('title'); // text
  $text_content  = get_sub_field('text_content'); // WYSIWYG
  $donation_form = get_sub_field('donation_form'); // textarea
  $mainsitegiveforms_select = get_sub_field('mainsitegiveforms_select'); // WYSIWYG
?>

<section id="donate" class="donation-block">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-6 pull-right">
        <div class="donation-content">
          <h3>Secure online donation</h3>
          <?php echo $donation_form; ?>

          <?php
            if($mainsitegiveforms_select && $mainsitegiveforms_select !== 'none') {
            // GiveWP donation form. 
              $shortcode = '';
              // switch_to_blog( 1 );
              // wp_reset_postdata();
                $shortcode = '[give_form id="' . $mainsitegiveforms_select . '" show_title="false" show_content="below" continue_button_title="Donate now"]';
                echo do_shortcode( $shortcode );
              // restore_current_blog();
              // wp_reset_postdata();

              echo '<p class="secure-note"><span class="fa fa-lock"></span> Secured by Stripe</p>';
            }
          ?>
          
        </div>
      </div>
      <div class="col-xs-12 col-sm-6 half-content">
        <h2 class="mb"><?php echo $title; ?></h2>
        <?php echo $text_content; ?>

      </div>
    </div>
  </div>
</section>