<?php
/**
 * Template part for displaying Hand and Halfs
 *
 * @package abcs
 */

?>

<?php
  $image_object  = get_sub_field('image'); // image
  $image_location  = get_sub_field('image_location'); // text
  $content  = get_sub_field('content'); // text
  $button_text  = get_sub_field('button_text'); // text
  $button_url  = get_sub_field('button_url'); // text
  $top_margin  = get_sub_field('top_margin'); // radio

  $section_class = (isset($top_margin) && $top_margin == 'none') ? 'npt' : '';
?>


<section id="halfhalf" class="half-and-half <?php echo $section_class; ?>">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-6 pull-<?php echo $image_location; ?>">
        <img src="<?php echo $image_object['sizes']['large']; ?>" class="half-image img-responsive">
      </div>
      <div class="col-xs-12 col-sm-6 half-content">
        <?php echo $content; ?></p>
        <?php if($button_text): ?>
          <a href="<?php echo $button_url; ?>" title="<?php echo $button_text; ?>" class="btn btn-primary"><?php echo $button_text; ?></a>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>