<?php
/**
 * Template part for displaying fullwidth text
 *
 * @package abcs
 */

?>

<?php
  $content  = get_sub_field('content'); // WYSIWYG
  $top_margin  = get_sub_field('top_margin'); // radio
  $bottom_margin  = get_sub_field('bottom_margin'); // radio
  $background_color  = get_sub_field('background_color'); // radio

  $section_class = (isset($top_margin) && $top_margin == 'none') ? 'npt' : '';
  $section_class .= (isset($bottom_margin) && $bottom_margin == 'none') ? ' npb' : '';
  $section_class .= ($background_color) ? ' ' . $background_color : '';
?>


<section id="fwtext" class="fullwidth-text <?php echo $section_class; ?>">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <?php echo $content; ?>
      </div>
    </div>
  </div>
</section>