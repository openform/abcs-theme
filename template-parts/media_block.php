<?php
/**
 * Template part for displaying Story Block
 *
 * @package abcs
 */

?>

<?php
  
  $size  = get_sub_field('size'); // radio
  $link_type  = get_sub_field('link_type'); // radio
  $blocks  = get_sub_field('blocks'); // repeater
  $background_color  = get_sub_field('background_color'); // radio

  if($background_color !== 'bg-white'){
    $background_color .= 'block-row-padded';
  }

  $size_class = 'col-sm-4';
  if ($size === 'quarters') {
    $size_class = 'col-sm-6 col-md-3';
  } elseif ($size === 'people') {
    $size_class = 'col-sm-4 col-md-3';
  } 
?>


<section id="mb" class="media-block-row <?php echo $background_color; ?>">
  <div class="container">
    <div class="row">
      <?php if( have_rows('blocks') ): ?>
        <?php while( have_rows('blocks') ): the_row(); 
            $image_object  = get_sub_field('image'); // image
            $title  = get_sub_field('title'); // text
            $excerpt  = get_sub_field('excerpt'); // textarea
            $link_text  = get_sub_field('link_text'); // text
            $block_link_type  = get_sub_field('block_link_type'); // Radio
            $link_url  = get_sub_field('link_url'); // URL
            $site_page_link  = get_sub_field('site_page_link'); // Page Link
            $allsiteslinks_select = get_sub_field('allsiteslinks_select'); // select
            $link_target  = get_sub_field('link_target'); // URL

            $page_link = '';
            if($block_link_type === 'url'){
              $page_link = $link_url;
            } 
            elseif($block_link_type === 'page'){
              $page_link = $site_page_link;
            } 
            elseif($block_link_type === 'multisite'){
              $page_link = $allsiteslinks_select;
            } 
        ?>

          <div class="col-xs-12 <?php echo $size_class; ?>">
            <article class="media-block <?php echo ($image_object ? '' : 'no-image'); ?>">
              
              <?php if($image_object): ?>
                <?php if($page_link !== ''): ?><a href="<?php echo $page_link; ?>" title="<?php echo $title; ?>" target="<?php echo $link_target; ?>"><?php endif; ?>
                <div class="b-lazy <?php echo ($size == 'people' ? 'img-aspect-4-5' : 'img-aspect-4-3'); ?>" style='background-image: url("<?php echo $image_object['sizes']['medium']; ?>; ?>"); ' data-src="<?php echo $image_object['sizes']['large']; ?>"></div>
                <!-- <img src="<?php echo $image_object['sizes']['medium']; ?>" alt="<?php echo $title; ?>" class="media-image b-lazy" data-src="<?php echo $image_object['sizes']['large']; ?>"> -->
                <?php if($page_link !== ''): ?></a><?php endif; ?>
              <?php endif; ?>

              <header class="media-text">
                <h1><?php echo $title; ?></h1>
                <p><?php echo $excerpt; ?></p>
                <?php if($link_text && $page_link !== ''): ?>
                  <?php if($link_type === 'text'): ?>
                    <a href="<?php echo $page_link; ?>" title="<?php echo $link_text; ?>" target="<?php echo $link_target; ?>" class="link-primary brand-bordercolor brand-texthovercolor"><?php echo $link_text; ?></a>
                  <?php else: ?>
                    <a href="<?php echo $page_link; ?>" title="<?php echo $link_text; ?>" target="<?php echo $link_target; ?>" class="btn btn-primary brand-buttoncolor brand-buttonhovercolor"><?php echo $link_text; ?></a>
                  <?php endif; ?>
                <?php endif; ?>
              </header>
            </article>
          </div>

        <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
</section>