<?php
/**
 * Template part for displaying Three Columns
 *
 * @package abcs
 */

?>

<?php
  $left  = get_sub_field('left'); // WYSIWYG
  $center  = get_sub_field('center'); // WYSIWYG
  $right  = get_sub_field('right'); // WYSIWYG
  $top_margin  = get_sub_field('top_margin'); // radio
  $background_color  = get_sub_field('background_color'); // radio

  $section_class = (isset($top_margin) && $top_margin == 'none') ? 'npt' : '';
  $section_class .= ($background_color) ? ' ' . $background_color : '';
?>


<section id="threec" class="three-columns <?php echo $section_class; ?>">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-4 left">
        <?php echo $left; ?>
      </div>

      <div class="col-xs-12 col-sm-4 center">
        <?php echo $center; ?>
      </div>

      <div class="col-xs-12 col-sm-4 right">
        <?php echo $right; ?>
      </div>
    </div>
  </div>
</section>