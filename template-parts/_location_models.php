<?php


//
// Returns Array
//
// building_services
//   building_id
//     ministry_id
//       ministry_name
//       url
//       services
//         id
  //         url
  //         locations
  //         name

class Services {
  public function all_services_by_building() {


    // Use cached list instead, 
    // to avoid overhead of the generation of this list.
    if (get_transient('services_by_building')) {
      return get_transient('services_by_building');
    }

    $services_filter = array();
    $building_services = array();

    // get all network websites
    $all_sites = get_sites( array(
      'orderby'=>'domain'
    ));

    foreach ( $all_sites as $site ):
      $ministry_id = '';

      // skip extra sites
      if($site->blog_id > 8){
        continue;
      }

      // set current post to current site
      switch_to_blog( $site->blog_id );

        // Add ministry to ministry filter
        $ministry_name = get_field('organization_name', 'option'); 
        $ministry_id = $site->blog_id;

        // get all ministry services
        $services_args = array( 
          'post_type' => 'services', 
          'posts_per_page' => -1,
          'orderby'    => 'title',
          'order'       => 'ASC',
        );
        $services_loop = new WP_Query( $services_args );
        if( $services_loop->have_posts() ):
          while ( $services_loop->have_posts() ) : $services_loop->the_post();
            $service_id = get_the_id();
            $service_locations = get_field('service_locations');
            $service_compound_id = $ministry_id . '-' . $service_id;
            $service_name = get_field('display_name', $service_id);
            $service_url = get_the_permalink( $service_id );

            foreach($service_locations as $location){
              if(!isset($location['location_site'][0])){
                continue;
              }
              $ministry_site_id = $location['location_site'][0];
              $ministry_site_url = get_the_permalink($ministry_site_id);
              $building_id = get_field('ministry_location_select', $ministry_site_id);

              if(!isset($building_services[$building_id])){
                $building_services[$building_id] = array();
              }
              if(!isset($building_services[$building_id][$ministry_id])){
                $building_services[$building_id][$ministry_id] = array(
                  'ministry_id' => $ministry_id,
                  'ministry_name' => $ministry_name,
                  'url' => get_site_url($ministry_id),
                  'location_url' => $ministry_site_url,
                  'services' => []
                );
              }

              // echo 'building ' . $building_id;
              // echo '<br/>min site ' . $ministry_site_id;
              // echo '<br/>ministry_name ' . $ministry_name;
              // echo is_array($building_services[$building_id][$ministry_id]['services']) . '<hr/>';

              $building_services[$building_id][$ministry_id]['services'][$service_id] = array(
                'url' => $service_url,
                'name' => $service_name,
                'locations' => get_field('service_locations'),              
              );
            }
          endwhile;
        endif;
      restore_current_blog();
    endforeach; // end network site loop
    

    // Store results for next time
    set_transient('services_by_building', $building_services, 60 * 12 * MINUTE_IN_SECONDS); // remember for 12 hours


    return $building_services;
  }
}

?>