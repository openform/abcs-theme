<?php
/**
 * Template part for displaying Locations Map
 *
 * @package abcs
 */

?>

<?php
  require_once(get_template_directory() . '/template-parts/_location_models.php');
  $serv = new Services;
  $building_services = $serv->all_services_by_building();

  // ABCS ONLY : All Websites Services List
  if(get_current_blog_id() === 1):
    $map_locations = array();
    $services_filter = array();
    $ministries_filter = array();

    // get all network websites
    $all_sites = get_sites( array(
      'orderby'=>'domain'
    ));

    // 
    // NETWORK SITES LOOP
    // each site and their services.
    //
    foreach ( $all_sites as $site ):
      $ministry_id = '';

      // skip extra sites
      if($site->blog_id > 8){
        continue;
      }

      // set current post to current site
      switch_to_blog( $site->blog_id );

        // Add ministry to ministry filter
        $ministry_name = get_field('organization_name', 'option'); 
        $ministry_id = $site->blog_id;
        if(!isset($ministries_filter[$ministry_id])){
          $ministries_filter[$ministry_id] = $ministry_name;
        }

        // get all ministry services
        $services_args = array( 
          'post_type' => 'services', 
          'posts_per_page' => -1,
          'orderby'    => 'title',
          'order'       => 'ASC',
        );
        $services_loop = new WP_Query( $services_args );
        if( $services_loop->have_posts() ):
          while ( $services_loop->have_posts() ) : $services_loop->the_post();
            $this_service = array(
              'ministry_id' => $ministry_id,
              'ministry_name' => $ministry_name,
              'service_id' => get_the_id(),
              'service_url' => get_the_permalink( get_the_id() ),
              'service_name' => get_field('display_name'),
              'service_locations' => get_field('service_locations'),
            );

            // Add service to service filter
            $service_compound_id = $ministry_id . '-' . $this_service['service_id'];
            if(!isset($services_filter[$service_compound_id])){
              $services_filter[$service_compound_id] = array(
                'name' => $this_service['service_name'],
                'locations' => $this_service['service_locations']
              );
            }

            foreach($this_service['service_locations'] as $location){
              if(!isset($location['location_site'][0])){
                continue;
              }
              $ministry_site_id = $location['location_site'][0];
              $building_id = get_field('ministry_location_select', $ministry_site_id);

              if(!isset($building_services[$building_id])){
                $building_services[$building_id] = array();
              }

              // $building_services[$building_id][$service_compound_id] = $this_service['service_name'];
            }

          endwhile;
        endif;
      restore_current_blog();
    endforeach; // end network site loop

    // Order the services alpha
    array_multisort( array_map('strtolower', array_column($services_filter, 'name')), SORT_ASC, $services_filter);
    

    // ABCS LOCATIONS LOOP
    $args = array( 
      'post_type' => 'locations', 
      'posts_per_page' => -1,
      'orderby'    => 'title',
      'order'       => 'ASC',
    );
    $loop = new WP_Query( $args );
    if( $loop->have_posts() ):
      while ( $loop->have_posts() ) : $loop->the_post();

        $building_id = get_the_id();

        $new_location = array(
          'display_name' => get_field('ministry_location_name', $building_id),
          'is_statewide' => get_field('statewide_office', $building_id),
          'url' => get_permalink( $building_id ),
          'phone' => get_field('ministry_location_phone', $building_id),
          'hours' => get_field('ministry_location_hours', $building_id),
          'building_id' => $building_id,
          'building_name' => get_field('display_name', $building_id),
          'building_phone' => get_field('phone', $building_id),
          'address' => get_field('address', $building_id),
          'city' => get_field('city', $building_id),
          'state' => get_field('state', $building_id),
          'zip' => get_field('zip', $building_id),
          'longitude' => get_field('longitude', $building_id),
          'latitude' => get_field('latitude', $building_id),
          'ministries_array' => array(),
          'ministries_ids' => array()
        );

        // Add Statewide locations to ministry array
        if(isset($new_location['is_statewide']) && $new_location['is_statewide'] === true){
          $this_min_service = 'state' . '-' . 'state';
          array_push($new_location['ministries_ids'], $this_min_service); 
        }

        // Add services to the Array
        if(isset($building_services[$building_id])){
          $new_location['ministries_array'] = $building_services[$building_id];

          foreach($building_services[$building_id] as $min_id => $min){
            if(isset($min['services']) && count($min['services']) > 0){
              foreach($min['services'] as $service_id => $service){
                $this_min_service = $min_id . '-' . $service_id;
                array_push($new_location['ministries_ids'], $this_min_service); 
              }
            }
          }
        }

        // Create Map List
        array_push($map_locations, $new_location);

      endwhile;
    endif; // end ABCS location loop

  endif; // end blog is abcs
?>

<section class="locations-map mb">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <form class="form-inline">
          <div class="form-group">
            <select id="serviceFilter" class="form-control select-filter">
                <option value="all">All Services</option>
              <?php foreach($services_filter as $service_id => $service): ?>
                <option value="<?php echo $service_id; ?>"><?php echo $service['name']; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <?php if(get_current_blog_id() === 1): ?>
            <div class="form-group">
              <select id="ministryFilter" class="form-control select-filter">
                  <option value="all">All Ministries</option>
                  <option value="s">Statewide Office</option>
                <?php foreach($ministries_filter as $ministry_id => $ministry_name): ?>
                  <option value="<?php echo $ministry_id; ?>"><?php echo $ministry_name; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          <?php endif; ?>
          <div class="form-group">
            <select id="geoFilter" class="form-control select-filter">
                <option value="all">All Distances</option>
                <option value="5">< 5 miles</option>
                <option value="10">< 10 miles</option>
                <option value="25">< 25 miles</option>
                <option value="100">< 100 miles</option>
            </select>
          </div>
        </form>
      </div>
      <div class="col-xs-12 col-sm-6 pull-right">
        <div id="map" class="filter-map">

          <?php foreach($map_locations as $f): ?>
            <location 
              data-longitude="<?php echo $f['longitude']; ?>" 
              data-latitude="<?php echo $f['latitude']; ?>" 
              data-name="<?php echo $f['building_name']; ?>" 
              data-note="" 
              data-url="<?php echo $f['url']; ?>" 
              data-address="<?php echo $f['address']; ?>" 
              data-services="<?php foreach($f['ministries_ids'] as $g){echo ' ms-' . $g; echo ' mn-' . substr($g, 0, 1);} ?>" 
              class="location active <?php foreach($f['ministries_ids'] as $g){echo ' ms-' . $g; echo ' mn-' . substr($g, 0, 1);} ?>">
            </location>
          <?php endforeach; ?>

        </div>
      </div>
      <div class="col-xs-12 col-sm-6">

        <?php if(count($map_locations) > 0): ?>
          <ul class="list-unstyled list-locations bt pt">

            <?php foreach($map_locations as $l): ?>
              <li class="<?php foreach($l['ministries_ids'] as $g){echo ' ms-' . $g; echo ' mn-' . substr($g, 0, 1);} ?> calc-distance" data-longitude="<?php echo $l['longitude']; ?>"  data-latitude="<?php echo $l['latitude']; ?>" >
                <a class="text-sans brand-texthovercolor" href="<?php echo $l['url']; ?>" title="View <?php echo $l['building_name']; ?>" >
                  <div class="location-left">
                    <h4><?php echo $l['building_name']; ?></h4>
                    <p class="text-sans"><?php echo $l['address']; ?>
                      <br/><?php echo $l['city'] . ', ' . $l['state'] . ' ' . $l['zip']; ?>
                      <br/><?php echo $l['building_phone']; ?>
                      <br/><span class="ministries">
                        <?php foreach($l['ministries_array'] as $min_id => $min){ 
                          echo $min['ministry_name'] . ', ';
                        } ?>
                      </span>
                    </p>
                  </div>
                </a>
              </li>
            <?php endforeach; ?>

          </ul>
        <?php endif; ?>

      </div>
    </div>
  </div>
</section>