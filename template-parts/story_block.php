<?php
/**
 * Template part for displaying Story Block
 *
 * @package abcs
 */

?>

<?php
  $image_object  = get_sub_field('image'); // image
  $eyebrow_text  = get_sub_field('eyebrow_text'); // text
  $title  = get_sub_field('title'); // text
  $button_text  = get_sub_field('button_text'); // text
  $button_url  = get_sub_field('button_url'); // text

  $button_link_type  = get_sub_field('button_link_type'); // Radio
  $link_url  = get_sub_field('link_url'); // URL
  $site_page_link  = get_sub_field('site_page_link'); // Page Link
  $allsiteslinks_select = get_sub_field('allsiteslinks_select'); // select
  $link_target = get_sub_field('link_target'); // radio

  $page_link = '';
  if($button_link_type === 'url'){
    $page_link = $link_url;
  } 
  elseif($button_link_type === 'page'){
    $page_link = $site_page_link;
  } 
  elseif($button_link_type === 'multisite'){
    $page_link = $allsiteslinks_select;
  } 
?>


<section id="story" class="story-block mb">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <article class="story-content">
          <a href="<?php echo $page_link; ?>" title="<?php echo $button_text; ?>" target="<?php echo $link_target; ?>" class="story-image">
            <img src="<?php echo $image_object['sizes']['large']; ?>" alt="<?php echo $title; ?>" >
          </a>
          <header class="story-text">
            <div>
              <p class="eyebrow"><?php echo $eyebrow_text; ?></p>
              <h1><?php echo $title; ?></h1>
              <?php if($button_text): ?>
                <a href="<?php echo $page_link; ?>" title="<?php echo $button_text; ?>" class="btn" target="<?php echo $link_target; ?>" ><?php echo $button_text; ?></a>
              <?php endif; ?>
            </div>
          </header>
        </article>
      </div>
    </div>
  </div>
</section>