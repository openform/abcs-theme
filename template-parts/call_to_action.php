<?php
/**
 * Template part for displaying Call to Action.
 *
 * @package abcs
 */

?>

<?php
  $text  = get_sub_field('text'); // text
  $button_text  = get_sub_field('button_text'); // text
  $button_url  = get_sub_field('button_url'); // page link
  $primary = get_sub_field('highlight_button'); // T/F

  
  $link_type  = get_sub_field('link_type'); // Radio
  $link_url  = get_sub_field('link_url'); // URL
  $site_page_link  = get_sub_field('site_page_link'); // Page Link
  $allsiteslinks_select = get_sub_field('allsiteslinks_select'); // select

  $btn_class = '';
  if($primary == '1') {
    $btn_class = 'btn-primary';
  }

  $page_link = '';
  $link_target = '';
  if($link_type === 'url'){
    $page_link = $link_url;
    $link_target = 'target="_blank"';
  } 
  elseif($link_type === 'page'){
    $page_link = $site_page_link;
  } 
  elseif($link_type === 'multisite'){
    $page_link = $allsiteslinks_select;
  }

?>


<section class="call-to-action">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8">
        <h2><?php echo $text; ?></h2>
      </div>
      <div class="col-xs-12 col-sm-4">
        <?php if($button_text && $page_link !== ''): ?>
          <p><a href="<?php echo $page_link; ?>" title="<?php echo $button_text; ?>" class="btn btn-block <?php echo $btn_class; ?>"><?php echo $button_text; ?></a></p> 
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>