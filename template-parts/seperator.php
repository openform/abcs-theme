<?php
/**
 * Template part for displaying seperation
 *
 * @package abcs
 */

?>
<?php
  $show_line  = get_sub_field('show_line'); // radio
  $height  = get_sub_field('height'); // radio
?>

<section class="seperator <?php if($height=='tall'){echo'mt mb';} ?>">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <?php if($show_line === 'yes'): ?>
          <hr/>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>