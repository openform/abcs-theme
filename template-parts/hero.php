<?php
/**
 * Template part for displaying Hero Images.
 *
 * @package abcs
 */

?>

<?php
  $hero_background_image_object  = get_sub_field('hero_background_image'); // image
  $hero_title  = get_sub_field('hero_title'); // text
  $hero_height  = get_sub_field('hero_height'); // select
  $text_color  = get_sub_field('text_color'); // select
  $image_position = get_sub_field('image_position'); // selet

  $hero_video_url = get_sub_field('hero_video_url') // url
?>


<section id="hero" class="hero hero-<?php echo $hero_height; ?>">
  <?php if($hero_height !== 'text'): ?>
    <div class="hero-bg dark b-lazy" style='background-image: url("<?php echo $hero_background_image_object['sizes']['large']; ?>"); <?php if($image_position){ echo "background-position: " . $image_position; } ?>' data-src="<?php echo $hero_background_image_object['sizes']['huge']; ?>"></div>

    <?php if($hero_video_url): ?>
      <video preload='auto' muted='true' poster="<?php echo $hero_background_image_object['sizes']['large']; ?>?background=1" id="bgvid" class="video-wait" loop>
        <source src="<?php echo $hero_video_url; ?>" type="video/mp4">
      </video>
      <button class="hide-video btn-video"><span class="fa fa-volume-off"></span></button>
    <?php endif; ?>
  <?php endif; ?>
  <div class="container">
    <div class="row">
      <div class="hero-content col-xs-12">
        <h1 class="<?php echo $text_color . ' ' . $hero_height; ?>"><?php echo $hero_title; ?></h1>
      </div>
    </div>
    <div id="bottom-of-hero"></div>
  </div>
</section>