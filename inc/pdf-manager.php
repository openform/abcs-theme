<?php

/**
 * Force PDF links anywhere to download
 */
if ( ! function_exists( 'force_pdf_open' ) ) :

function force_pdf_open() {

  // check nonce for security.
  if( isset( $_POST['pdf_add_user_meta_nonce'] ) && wp_verify_nonce( $_POST['pdf_add_user_meta_nonce'], 'pdf_add_user_meta_form_nonce') ) {

    // sanitize the input
    $pdf_url = sanitize_text_field( $_POST['pdf_url'] );
    $filename = basename($pdf_url);
    $pdf = file_get_contents($pdf_url);


    function outputPdf( $fileName, $pdf ) {
        status_header(200);
        ob_clean();
        header( 'Pragma: public' );
        header( 'Expires: 0' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
        header( 'Cache-Control: private', false );
        header( 'Content-Type: application/pdf' );
        header( 'Content-Disposition: attachment; filename=' . $fileName );
        flush(); 
        echo $pdf;
    }

    outputPdf($filename, $pdf);

    exit; // This is really important - otherwise it shoves all of your page code into the download
  }
}
endif;
add_action("admin_post_force_pdf_open", "force_pdf_open");
add_action("admin_post_nopriv_force_pdf_open", "force_pdf_open");



/**
 * Build the PDF download form to Force PDF links anywhere to download
 */

if ( ! function_exists( 'force_pdf_open_form' ) ) :

function force_pdf_open_form() {
  // Generate a custom nonce value.
  $pdf_add_meta_nonce = wp_create_nonce( "pdf_add_user_meta_form_nonce" );

  echo '
  <form method="post" id="force_pdf_open" action="' . esc_url( admin_url("admin-post.php") ) . '">
      <input type="hidden" name="pdf_add_user_meta_nonce" value="' . $pdf_add_meta_nonce . '" />
      <input type="hidden" name="action" value="force_pdf_open">
      <input type="hidden" name="pdf_url" id="pdf_download_url_value" value="" />
  </form>
  ';
}
endif;
