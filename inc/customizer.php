<?php
/**
 * caring Theme Customizer
 *
 * @package abcs
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function caring_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'caring_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'caring_customize_partial_blogdescription',
		) );
	}
}
add_action( 'customize_register', 'caring_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function caring_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function caring_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function caring_customize_preview_js() {
	wp_enqueue_script( 'caring-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'caring_customize_preview_js' );



/**
 * Custom Code section for footer
 */
function allow_script_tags($input) {
    return $input;
}
function abcs_custom_footer_code_customize_register($wp_customize) {
    // Step 1: Create Customizer Section
    $wp_customize->add_section('custom_footer_code_section', array(
        'title'    => __('Footer Custom Code', 'abcs'),
        'priority' => 30,
    ));

    // Step 2: Add Customizer Setting
    $wp_customize->add_setting('custom_footer_code', array(
        'default'           => '',
        'sanitize_callback' => 'wp_kses_post',
        'sanitize_callback' => 'allow_script_tags',
    ));

    // Step 3: Add Customizer Control
    $wp_customize->add_control('custom_footer_code_control', array(
        'label'    => __('Custom Code', 'abcs'),
        'section'  => 'custom_footer_code_section',
        'settings' => 'custom_footer_code',
        'type'     => 'textarea',
        'description' => __('Add your custom code here. Note: Use caution and only include trusted code.', 'your-theme-textdomain'),

    ));
}
add_action('customize_register', 'abcs_custom_footer_code_customize_register');

