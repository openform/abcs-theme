<?php
/**
 * Functions to create shortcodes for sitewide use.
 *
 * @package abcs
 */


function form_display_prayer_requests($atts = array()) {  
  // set up default parameters
  extract(shortcode_atts(array(
   'form_id' => 1,
   'field_id' => 1
  ), $atts));

  $results = '<ul class="list list-unstyled list-prayer">';

  // FORM SETUP
  $form = GFAPI::get_form($form_id);
  $entries = GFAPI::get_entries( $form_id );
  foreach($entries as $e):

    // show only active, unprocessed registraitons
    if($e['status'] !== 'active')
      continue;

    // var_dump($e);

    date_default_timezone_set('UTC');
    $created_date = strtotime($e['date_created']);
    date_default_timezone_set('America/Phoenix');
    $results .= '<li>' . $e[$field_id] . '<br/><small>' . date('F jS, h:i a', $created_date) . '</small></li>';

  endforeach;

  $results .= '</ul>';
    
  return $results;
  
}
add_shortcode( 'form_display_prayer_requests', 'form_display_prayer_requests' );
