<?php
/**
 * Restores the ability for Administrators (NOT SUPER ADMINs)
 * on a Multisite setup
 * to add embeds and scripts to posts/pages.
 * this capability is removed in MUs by default as a security precaution.
 * 
 * REF: https://gist.github.com/kellenmace/9e6a6fbb92ec75940f23d2a6f01c9b59
 */


function multisite_restore_unfiltered_html( $caps, $cap, $user_id, ...$args ) {
  if ( 'unfiltered_html' === $cap && user_can( $user_id, 'administrator' ) ) {
    $caps = array( 'unfiltered_html' );
  }

  return $caps;
}

add_filter( 'map_meta_cap', 'multisite_restore_unfiltered_html', 1, 4 );