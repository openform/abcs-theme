

LEFT OFF

the Hide hours doesnt seem to be working to hide the hours on the New life site. 
  so i disabeld the hours on the abcs building to hide it. 





TASKS

1. Add checkbox to TURN OFF hours on a ministry site
  will override location. 

2. Change link on ABCS site location page from the ministry homepage to the ministry individual location page. 
  Start here: https://abcs.org/location/phoenix-office/
  Current goes to: https://azresourcecenters.com/
  Change to go here: https://azresourcecenters.com/locations/bhhs-legacy-foundation-resource-center/














---------------------
CALENDER - Chellsea, March 18, 2021
---------------------

When adding contact information (name, phone number, email and website link) only the name and email address come up.

  On the single event page:
    - phone not showing: corrected.
    - website not showing: corrected.

If someone only adds a phone number, no contact information is shown and it is replaced with the description.

  - resolved with the above


If there’s only a name and phone number available, the phone number needs to be put in the place of an email in order for the information to show.

  - resolved with the above


When choosing between the location options the following occurs:

    -“ABCS Location” sets the location to a comma (,) instead of the Tucson address

      *This we were not able to see. 


    -“Another Location” doesn’t show the custom location city

      City not showing: resolved.


    - The city name has to be included in the “Custom location address” in order for it to appear in the events page

      - resolved with above.


    -There is no option to leave the location blank
    NLPC currently has a virtual class happening and there is no way to add “virtual” to the location without needing a permanent address

      - New virtual event option now available,
      This will hide the location info, and just show "virtual event"


When you are looking at the Classes Calendar (list view)
-----------------------------------------------------------

The description appears on the fourth column and cuts off information

  - description cut off, 
  True. This is intentional because the descriptions can be very large. 


The description cuts off with ellipse dots and you cannot view more of the description when clicking on them

  - view more
  true. you can view more on the event detail page. We added a link to the description elipsis so it's clear how to read more.



The location either shows the address or the comma as mentioned above.

  - this we are not able to reproduce. The comma specifically.



Could we have the contact information show instead of the cut-off description?

  - Sure.
  Would you always want the contact info instead? It's either or for all ABCS websites. 
  And would you want all contact info? Name, email, phone, URL.

  It seems this might be a little bit much to put here. The User experience at this level is finding events they heard about or are discovering. So at this point, they are learning if they wwant to know more. The next step of this is digging one step deeper, to learn even more and possible sign up. That's what the detail page provides. ALl the details. 

  Something else that is common is having a separate field for a short description. It would limit the characters and if you enter that short description it would appear here. If you left it blank it would do the same it is doing now. This might be a better solution for you to have a little more control over the description area on the list view to use as you see fit for specifica cases, without making a balnket switch to all the ABCS sites for this area. 

















---------------------
BONUS - ok to finish after deployment
---------------------

#1 Calendars-
DONE - custom address for event (not min site nor location)
  DONE check which type
  DONE set external if chosen
  DONE location needs name and address
  DONE hide map on event pgae if external - no lat long
    DONE or get the lat long?? I dont' know. Lame. But needed.

#3 Locations
DONE - add field to Locaiton - indicate if statewide office (checkbox)
  DONE - look into adding "Statewide office" as a ministry option.
- show the statewide offices at the top of the ABCS location page. - omit ministry list on line 5.
- add field to location to select MAIN ministry org. Then show this ministry name at the front of the location title on the locations page if on ABCS site. Otherwise show only location title.

Add Modules for:
DONE #2 - three column WYSIWYG
DONE - last three blog posts. (as a shortcode)
  NOTE - not a schortcode - a module now. 
#5 - calendar


adjust Typekit to remove dev domain and add all others.
  May need to move this to Roberts???
    Dolly
    Century Gothic





---------------------
NOTES
---------------------

Events
  We will often post events that are not taking place at an ABCS location.  Can we add separate addresses?
  
  If we add a picture to the event it does not show up in the preview, and some of the contact information like phone number or event website do not appear.  (See Reckless Love: Caring for Teens on the foster care website as an example.)
  
  Using the Reckless Love event as an example, if we wanted a simpler direct URL to use such as “abcsfostercare.com/teens” to go to the event page, how would we do that?  Trying to customize the event page URL limits us to the events directory.  Would we create a page in the root directory called “teens” and use that to redirect to the event page?



Where I left off:

  give select works, shows form on other sites, 
  but when attempting to submit the donation, it errors with incorrect max value. 
  submitted support ticket.\

    Fallback plan:
      create a single donate form page on ABCS.
      use query string to show site logo and back nav - no other nav
      have the give form, and maybe indicate the ministry (if that matters)
      think through where the donation confirmation goes, and how that page looks
        may need to modify this template so that the donation information shows nicely and also still appears to be branded with that ministry. Hmm. sure there is a way. 

        
  
  stripe integration
    donation confirmation and other pages setup.. Copy GRM and Caring

    


Finish Stripe setup and WP GIve config.
  1. connect stripe to wp-give = needs stripe admin permissions
  2. test stripe
  3. Implement on all sites - export/import give form.
  4. test on all sites.
  5. take out of test mode on all sites.

  Setup one master give form - 

      






DEPLOY - AFTER
------------------

NO wp rocket
wp speed test

DONE google web console
  DONE wait for ssl
  DONE add all sites
  DONE share users w chris
  DONE add sitemaps


DONE add Recaptcha keys to all domains Gravity Forms Settings pages.
  site: 6LeZFYMUAAAAANzWEavos3a7cTILkkh8XHnYMRI6
  secret: 6LeZFYMUAAAAAMVfn_qCY0w2B_h2wwzgfqWPKfWq

DONE Create Redirections:
      ABCS
      X abcs.org/taxcredit
      X abcs.org/donate
      X abcs.org/givingtuesday  
      X abcs.org/blog
      X abcs.org/legacy              
      ? abcs.org/pray 
      X abcs.org/volunteer
      ? abcs.org/wishlist
      X abcs.org/connect
      ? abcs.org/churches
      ? abcs.org/impactaz
      X abcs.org/support
      ?abcs.org/christmas (YEM)
       
      NLPC
      X newlifepregnancy.com/pregnant
      X newlifepregnancy.com/services
      ? newlifepregnancy.com/bbc
      ? newlifepregnancy.com/sohl
      ? newlifepregnancy.com/mdo
      ? newlifepregnancy.com/foam5K
      ? newlifepregnancy.com/jingle
      X newlifepregnancy.com/donate
      ? newlifepregnancy.com/golf
       
      Arms of Love
      X abcsfostercare.com/donate
      X abcsfostercare.com/endowment
      ? abcsfostercare.com/fundraisers
      ? abcsfostercare.com/5KforFC
       
      PLM
      X pursuelifeaz.com/donate
      ? pursuelifeaz.com/compass
      ? pursuelifeaz.com/workshops
      X pursuelifeaz.com/travel
      ? pursuelifeaz.com/medicare
      ? pursuelifeaz.com/help
      X pursuelifeaz.com/resources
      X pursuelifeaz.com/articles
      ? pursuelifeaz.com/caregivers
       
      Leader Care
      ?? leadercareaz.com/donate. (.com???) Alsot the donate page is not populated
       
      CRCs
      X azresourcecenters.com/donate
       
      Rio Vista Center
      ? riovistacenter.com/turkeyday
      ? riovistacenter.com/backtoschool (Casa de Amor)
      X riovsitacenter.com/donate
       
      NLTP
      X newlifetransitional.com/donate
     
       
       







Handoff
---


User accounts
  web
  google

training 
tutorial articles

Next steps

Hosting setup notification









---------------------
DONE
---------------------

DONE google analytics
  have it. but may need to setup cross-domain tracking - URGH

DONE Email sending setup SMTP
  double check configed on all sites.

DONE Locations
- confirm do we add 'BUILDINGS'  only on the ABCS site??
  - if so, can we hide Buildings on other sites? so it's not confusing. 
  Yes. Will Hide. 

DONE arch page
  DONE needs styling like 404 page.

DONE Get David Access
  user account not working

Footer
CHRIS CHECKING ABCS - make sure newsletter form is working
DONE ABCS - might need to update the form code for ABCS instead of Caring lists. 

Spacing
DONE - add spacer item

Calendar-
TEST - contact person phone doesn't show
  - shows for me, might just need a new update on the live site.
DONE - add page modules below top section to events detail page
- another task - see paper - confusing.
DONE - arrows on calendar layout darkerr, and today button
DONE - events admin page columns not working

Locations Map (both template files)
DONE - alpha order services array list
DONE - double check location distances - need to reset my browser to get a clean UX for testing.
DONE - figure out how to unblock my location.
  - localhost has to be https for geolocation to work!!

About Page.
DONE - check that we can link a media block to a Staff Page
  DONE _ only links to staff in this same ministry, not others. that would need to be a URL
DONE - also make sure the link shows.

Blog
DONE - blog page on ABCS - is missing all the posts. how is the archive working???
- show categories on blog article pages - as links. 
DONE - flip the blog nav L/R - future is backwards, past is forwards 

Staff Detail Page.
DONE - ensure everything shows. bio, image, etc.

Media Block row
DONE - add option to open link in new tab.

Location page
DONE - show staff image if available.
DONE - show bio too
  Note: Staff muct be add to ministry site to appear.

Calendars-
DONE - Default view if no events, calendar layout not list

Add ID Block thingy like Eagle Lake
DONE - for each type of module, add an ID
DONE - show it if logged in.
  May need to fill a few gaps here depending when it's needed.

DONE Links
  Lead, story, media, etc.
  add field for option to open in new tab. 
  make this a choice if you are on URL or Any Multisite page.
  
DONE Smush
  I know we’re using Smush to optimize image size.  We’re seeing some images may be overly downgraded where it’s slightly pixelated, where others are fine.  Is there a rule of thumb for us to follow to make sure images quality is what we’re expecting?

DONE Story Block
  Something happened to the Story Block where the image is now a little short vertically within the gray box.

DONE Text Block
  In a Text Block, we used a bulleted section of text and inserted hyperlinks.  They defaulted to some master style to present in all caps.  Is there a way to remove that?  Check http://missionencounter360.abcs.flywheelsites.com/churches/ in the bullets under Pregnancy Care.
