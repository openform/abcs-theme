<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package abcs
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png; ?>">
  
  <link rel="stylesheet" href="https://use.typekit.net/vje8nyv.css">
  
  <?php // if map page  ?>
  <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.44.2/mapbox-gl.js'></script>
  <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.44.2/mapbox-gl.css' rel='stylesheet' />

  
  <?php wp_head(); ?>


  <?php 
    $logo_image_object = get_field('logo', 'option'); 
    $logo_small_image_object = get_field('logo_small', 'option'); 
    $logo_icon_object = get_field('logo_icon', 'option'); 

    $primary_color = get_field('primary_color', 'option');

    $this_site_id = get_current_blog_id();
    $super_text = 'A Ministry of Arizona Baptist Children\'s Services';
    $super_text_mobile = 'A Ministry of ABCS';
    if($this_site_id === 1) {
      $super_text = '7 Ministries, 1 Mission';
      $super_text_mobile = '7 Ministries, 1 Mission';
    }

    $all_sites = get_sites( array(
      'site__not_in'=>array(1),
      'orderby'=>'domain'
    ));
  ?>
  
  <style>
    .brand-bordercolor { border-color: <?php echo $primary_color; ?> !important; }
    .brand-textcolor { color: <?php echo $primary_color; ?> !important; }
    .brand-texthovercolor:hover, .brand-texthovercolor:focus { color: <?php echo $primary_color; ?> !important; }
    .brand-active-textcolor.active > a { color: <?php echo $primary_color; ?> !important; }
    .brand-buttoncolor { background: <?php echo $primary_color; ?> !important; }
    .brand-buttonhovercolor:hover, .brand-buttonhovercolor:focus { background: <?php echo $primary_color; ?> !important; }
    .btn-primary { background: <?php echo $primary_color; ?> !important; }
    .btn-primary:hover, .btn-primary:focus { background: #444444 !important; }
    .nav-collapse .nav-top ul.nav li a:hover { border-color: <?php echo $primary_color; ?> !important; color: <?php echo $primary_color; ?> !important; }
    .footer ul.nav li a:hover, .footer ul.nav li a:focus { color: <?php echo $primary_color; ?> !important; } 
    .marker { color: <?php echo $primary_color; ?> !important; }
    @media (min-width: 768px) {
      .nav-collapse .nav-top ul.nav li.current-menu-item a { border-color: <?php echo $primary_color; ?> !important; }

      /* Make last menu item (donate) a button */
      .nav-collapse .nav-top ul.nav li:last-of-type { padding: 8px 2px; }
      .nav-collapse .nav-top ul.nav li:last-of-type a { padding: 2px 8px; background: <?php echo $primary_color; ?> !important; color: white; }
      .nav-collapse .nav-top ul.nav li:last-of-type a:hover { background: #444444 !important; color: #fff !important; }
    }
    @media (max-width: 767px) {.nav-collapse .nav-top ul.nav li.current-menu-item a { color: <?php echo $primary_color; ?> !important; }}
    p a, li a { color: <?php echo $primary_color; ?>; }
    p a:hover, p a:focus, li a:hover, li a:focus { color: <?php echo $primary_color; ?>; }
    ul.list-locations>li>a h4 { color: <?php echo $primary_color; ?>; }


  </style>


  <?php
    // Infinite Media Tracking Pixel
    // installed Feb 28, 2025 per Chellsea
  ?>
  <script type="text/javascript">
      window._adftrack = Array.isArray(window._adftrack) ? window._adftrack : (window._adftrack ? [window._adftrack] : []);
      window._adftrack.push({
          pm: 3289704
      });
      (function () { var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://a2.adform.net/serving/scripts/trackpoint/async/'; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); })();
  </script>
  <noscript>
      <p style="margin:0;padding:0;border:0;">
          <img src="https://a2.adform.net/Serving/TrackPoint/?pm=3289704" width="1" height="1" alt="" />
      </p>
  </noscript>
  <?php // end Infinite Media Tracking Pixel ?>


</head>


<body <?php body_class(); ?> data-brandcolor="<?php echo $primary_color; ?>">
<div id="page" class="site">
  <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'caring' ); ?></a>

  <!-- header -->
    <header class="site-header fixed <?php if(is_front_page()){ echo 'home-page'; } ?>">
      <div class="super-nav">
        <div class="top">
          <div class="container">
            <a href="#" class="super-link hidden-xs"><?php echo $super_text; ?> <span class="arrow-down"></span></a>
            <a href="#" class="super-link visible-xs"><?php echo $super_text_mobile; ?> <span class="arrow-down"></span></a>
          </div>
        </div>
        <div class="content">
          <div class="container">
            <div class="row">
              <div class="col-sm-4">
                <div class="row">
                  <div class="col-sm-9 col-md-6">
                    <?php 
                      switch_to_blog( 1 );
                      $blog_url = "";
                      $blog_url = str_replace('/es', '', get_bloginfo('url'));
                    ?>
                      <a class="no-translate" href="<?php echo $blog_url; ?>" title="View <?php echo get_bloginfo('name'); ?>" data-no-translation-href><?php echo get_bloginfo('name'); ?></a>
                    <?php 
                      restore_current_blog();
                    ?>
                  </div>
                </div>
              </div>
              <div class="col-sm-8">
                <div class="row">
                  <?php 
                    foreach ( $all_sites as $site ):
                      if($site->blog_id > 8){
                        continue;
                      }
                      switch_to_blog( $site->blog_id );
                      $blog_url = "";
                      $blog_url = str_replace('/es', '', get_bloginfo('url'));
                  ?>

                      <div class="col-sm-3">
                        <a class="no-translate" href="<?php echo $blog_url; ?>" title="View <?php echo get_bloginfo('name'); ?>" data-no-translation-href><?php echo get_bloginfo('name'); ?></a>
                      </div>

                  <?php 
                      restore_current_blog();
                    endforeach;
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="top-nav" class="top-nav ">
        <nav class="navbar" role="navigation">
          <div class="container">

            <div class="row">
              <div class="col-xs-8 col-sm-3">
                <div class="navbar-header">
                  <a class="brand" href="<?php echo home_url(); ?>">
                    <img src="<?php echo $logo_image_object['sizes']['medium']; ?>" width="170" alt="<?php echo get_bloginfo('name'); ?> logo" class=""/>
                  </a>
                </div>
              </div>
              <div class="col-xs-4 col-sm-9 text-right">

                <div class="nav-open text-right visible-xs pull-right">
                  <button id="open-nav" type="button" class="btn-menu">
                    <span class="lines-circle"><span class="line-1"></span><span class="line-2"></span><span class="line-3"></span></span>
                  </button>
                </div>

                <div class="nav-collapse" >
                  <div class="nav-top">
                    <?php
                      $args = array(
                        'menu' => 'top-nav',
                        'menu_class' => 'nav',
                        'menu_id' => 'navigation-top',
                        'container' => false,
                        'theme_location' => 'primary'
                      );
                      wp_nav_menu($args);
                    ?>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </nav>
      </div>
    </header>

  <div id="content" class="site-content <?php if( current_user_can('edit_others_pages') ) { echo 'admin-viewer'; }  ?>">
    <div id="content-marker" ></div>
