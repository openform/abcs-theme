<?php
/**
 * abcs functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package abcs
 */

define('TEMPLATE_DIR', get_template_directory());

if ( ! function_exists( 'abcs_setup' ) ) :


  // ACF Settings page
  if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
      'page_title'  => 'Ministry Settings',
      'menu_title'  => 'Ministry Settings',
      'menu_slug'   => 'theme-general-settings',
      'capability'  => 'edit_posts',
      'redirect'    => false
    ));
  }

	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function abcs_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on abcs, use a find and replace
		 * to change 'abcs' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'abcs', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );


    /**
     * Custom Image Sizes
     *
     * OPENFORM
     */
    add_image_size( 'huge', 2000, 2000 );
    // add_image_size( 'featured_preview', 55, 55, true);

    /**
     * Custom Network Media Library - central site media setting
     *
     * OPENFORM
     */
    // add_filter( 'network-media-library/site_id', function( $site_id ) {
    //     return 1;
    // } );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'abcs' ),
      'menu-2' => esc_html__( 'Footer 1', 'abcs' ),
      'menu-3' => esc_html__( 'Footer 2', 'abcs' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

	}
endif;
add_action( 'after_setup_theme', 'abcs_setup' );


/**
 * Enqueue scripts and styles.
 */
function abcs_scripts() {

	wp_enqueue_script( 'abcs-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_enqueue_script( 'jquery', get_stylesheet_directory_uri() . '/js/jquery-1.9.1.min.js', array(), '1.9.1', true );
  wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.2', true );
  wp_enqueue_script( 'blazy', get_stylesheet_directory_uri() . '/js/blazy.min.js', array(), '1.3.1', true );
  wp_enqueue_script( 'waypoints', get_stylesheet_directory_uri() . '/js/waypoints.min.js', array(), '2.0.5', true );
  wp_enqueue_script( 'moment', get_stylesheet_directory_uri() . '/js/moment.js', array(), '2.12.0', true );
  wp_enqueue_script( 'fullcalendar', get_stylesheet_directory_uri() . '/js/fullcalendar.min.js', array('moment'), '3.4.0', true );
 
  wp_enqueue_script( 'script', get_stylesheet_directory_uri() . '/js/scripts.js?1=1', array('jquery','blazy','bootstrap'), '1.1.16', true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'abcs_scripts' );



function abcs_admin_style() {
        wp_register_style( 'abcs_admin_style', get_template_directory_uri() . '/css/admin/admin.css', false, '1.0.0' );
        wp_enqueue_style( 'abcs_admin_style' );
}
add_action( 'admin_enqueue_scripts', 'abcs_admin_style' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Shortcodezzz.
 */
require get_template_directory() . '/inc/shortcodes.php';

/**
 * PDF STuff.
 */
require get_template_directory() . '/inc/pdf-manager.php';

/**
 * ADMIN Multisite editor STuff.
 */
require get_template_directory() . '/inc/admin-role-filters.php';




/**
 * Translate Press 
 * 
 * remove external links from translation
 * avoid appending '/es' to other ABCS sites links. 
 */
function trpc_add_new_substring_to_be_skipped_from_dynamic_translation($substrings){
    $excluded_substrings = array( 
      'abcs.org',
      'abcsfostercare.com',
      'azresourcecenters.com',
      'missionencounter360.com',
      'newlifeaz.org',
      'newlifepregnancy.com',
      'newlifetransitional.com',
      'pursuelifeaz.com',
      'abcs.local',
      'newlife.abcs.local'
    );
    $substrings['href'] = array_merge($excluded_substrings, $substrings['href']);
    return $substrings;
}
add_filter( 'trp_skip_strings_from_dynamic_translation_for_substrings', 'trpc_add_new_substring_to_be_skipped_from_dynamic_translation', 10, 1);





/**
 * Enqueue scripts and styles.
 */
function abcs_styles() {
  $use_grunt_dist = false;

  if ($use_grunt_dist){
    // If `grunt dist` is run to package the final css, use this stylesheet
    wp_enqueue_style( 'main_dist_styles', get_stylesheet_directory_uri() . '/css/tidy.min.css', array(), '20181021' );
  }
  else{
    // If just `grunt watch` is being used, use these stylesheets
    wp_enqueue_style( 'main_styles', get_stylesheet_directory_uri() . '/css/styles.css', array(), filemtime( TEMPLATE_DIR . '/css/styles.css' ) );
  }
}
add_action( 'wp_enqueue_scripts', 'abcs_styles' );




/*
 * Make all comment notifications go to specific email
 */
// add_filter('comment_notification_recipients', 'override_comment_notice_repicient', 10, 2);
function override_comment_notice_repicient($emails, $comment_id) {

  $comment = get_comment( $comment_id );
  if ( empty( $comment ) )
    return $emails;

  return array('trash@trash.com');
}





/*
 * Hide Admin Bar
 */
add_filter('show_admin_bar', '__return_false');


/*
 * Login Logo
 *
 * Ref: https://codex.wordpress.org/Customizing_the_Login_Form
 */
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/ABCS-full.png);
            padding-bottom: 30px;
            width: 100%;
            background-size: contain;
        }
        body.login {
          background: #fff;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );
/*
 * Admin bar Logo
 *
 * Ref: http://wordpress.stackexchange.com/questions/126303/replace-admin-bar-logo
 */
function custom_admin_bar(){
    echo '
        <style type="text/css">
            /* Admin bar */
            #wpadminbar {
              background: #88898D;
              border-bottom: 1px solid #999;
            }
            /* Topbar text */
            #wpadminbar .ab-empty-item, #wpadminbar a.ab-item, #wpadminbar>#wp-toolbar span.ab-label, #wpadminbar>#wp-toolbar span.noticon {
              color: #fff;
            }
            /* Topbar icon */
            #wpadminbar .ab-icon:before,
            .wp-admin #wpadminbar #wp-admin-bar-site-name>.ab-item:before {
              color: #777;
            }
            /* Topbar sitename text - Hover */
            #wpadminbar .ab-top-menu>li.hover>.ab-item, #wpadminbar.nojq .quicklinks .ab-top-menu>li>.ab-item:focus, #wpadminbar:not(.mobile) .ab-top-menu>li:hover>.ab-item, #wpadminbar:not(.mobile) .ab-top-menu>li>.ab-item:focus {
              color: #F9423A;
            }
            /* Topbar text - Hover */
            #wpadminbar:not(.mobile)>#wp-toolbar a:focus span.ab-label, #wpadminbar:not(.mobile)>#wp-toolbar li:hover span.ab-label, #wpadminbar>#wp-toolbar li.hover span.ab-label {
              color: #F9423A;
            }
            /* Topbar icon - Hover */
            #wpadminbar .quicklinks .ab-sub-wrapper .menupop.hover>a, #wpadminbar .quicklinks .menupop ul li a:focus, #wpadminbar .quicklinks .menupop ul li a:focus strong, #wpadminbar .quicklinks .menupop ul li a:hover, #wpadminbar .quicklinks .menupop ul li a:hover strong, #wpadminbar .quicklinks .menupop.hover ul li a:focus, #wpadminbar .quicklinks .menupop.hover ul li a:hover, #wpadminbar .quicklinks .menupop.hover ul li div[tabindex]:focus, #wpadminbar .quicklinks .menupop.hover ul li div[tabindex]:hover, #wpadminbar li #adminbarsearch.adminbar-focused:before, #wpadminbar li .ab-item:focus .ab-icon:before, #wpadminbar li .ab-item:focus:before, #wpadminbar li a:focus .ab-icon:before, #wpadminbar li.hover .ab-icon:before, #wpadminbar li.hover .ab-item:before, #wpadminbar li:hover #adminbarsearch:before, #wpadminbar li:hover .ab-icon:before, #wpadminbar li:hover .ab-item:before, #wpadminbar.nojs .quicklinks .menupop:hover ul li a:focus, #wpadminbar.nojs .quicklinks .menupop:hover ul li a:hover {
              color: #F9423A;
            }
            /* Topbar WP logo remove */
            #wp-admin-bar-wp-logo {
              display: block;
            }
            /* Custom Logo */
            #wp-admin-bar-wp-logo .ab-icon:before{ content:"" !important; }
            #wp-admin-bar-wp-logo .ab-icon,
            #wpadminbar>#wp-toolbar>#wp-admin-bar-root-default #wp-admin-bar-wp-logo .ab-icon { 
              background-image: url(' . get_stylesheet_directory_uri() . '/favicon.png) !important; 
              background-size: cover;
              background-repeat: cover;
              margin-top: 4px;
              padding: 0 !important;
              width: 30px;
              height: 30px;
              border-radius: 100%;
            }
            /* Left nav - Active Menu item */
            #adminmenu .wp-has-current-submenu .wp-submenu .wp-submenu-head, #adminmenu .wp-menu-arrow, #adminmenu .wp-menu-arrow div, #adminmenu li.current a.menu-top, #adminmenu li.wp-has-current-submenu a.wp-has-current-submenu, .folded #adminmenu li.current.menu-top, .folded #adminmenu li.wp-has-current-submenu {
              background: #F9423A;
              color: #222222;
              font-weight: bold;
              text-transform: uppercase;
            }
            /* Left nav - Active Menu item text color */
            #adminmenu .current div.wp-menu-image:before, #adminmenu .wp-has-current-submenu div.wp-menu-image:before, #adminmenu a.current:hover div.wp-menu-image:before, #adminmenu a.wp-has-current-submenu:hover div.wp-menu-image:before, #adminmenu li.wp-has-current-submenu a:focus div.wp-menu-image:before, #adminmenu li.wp-has-current-submenu.opensub div.wp-menu-image:before, #adminmenu li.wp-has-current-submenu:hover div.wp-menu-image:before {
              color: #222222;
            }
            /* Left nav - Menu Alert Badge */
            #adminmenu .awaiting-mod, #adminmenu .update-plugins {
              background: #F9423A;
            }
            /* Left nav - Hover Menu item text color*/
            #adminmenu li.menu-top:hover, #adminmenu li.opensub>a.menu-top, #adminmenu li>a.menu-top:focus,
            #adminmenu .wp-submenu a:focus, #adminmenu .wp-submenu a:hover, #adminmenu a:hover, #adminmenu li.menu-top>a:focus {
              color: #F9423A;
            }
            /* Left nav - Hover Menu Item icon color */
            #adminmenu li a:focus div.wp-menu-image:before, #adminmenu li.opensub div.wp-menu-image:before, #adminmenu li:hover div.wp-menu-image:before {
              color: #F9423A;
            }
            /* Left nav background */
            #adminmenu, #adminmenu .wp-submenu, #adminmenuback, #adminmenuwrap {
              background: #222222;
            }
            /* Admin buttons */
            .wp-core-ui .button-primary,
            .wp-core-ui .button.button-primary.button-hero {
              background: #F9423A;
              border-color: #F9423A;
              box-shadow: none;
              text-shadow: none;
            }
            .wp-core-ui .button-primary.focus, .wp-core-ui .button-primary.hover, .wp-core-ui .button-primary:focus, .wp-core-ui .button-primary:hover {
              background: #F0413B;
              border-color: #F0413B;
              box-shadow: none;
              text-shadow: none;
              text-decoration: underline;
            }
            /* FOOTER 
             **********/

            /* Footer Link */
            #footer-left a {
              color: #F9423A;
              text-decoration: none;
            }
            #footer-left a:hover {
              color: #F9423A;
              text-decoration: underline;
            }
        </style>
    ';
}
add_action( 'admin_head', 'custom_admin_bar' );




/*
    Excerpt
*/
// Length
function custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
function new_excerpt_more($more) {
    global $post;
    return '<a class="moretag" href="'. get_permalink($post->ID) . '">...</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');


/*
 * ACF Multisite - share repeater values from main site
 *
 * Ref: https://www.advancedcustomfields.com/resources/dynamically-populate-a-select-fields-choices/
 */
function acf_load_servicelocation_field_choices( $field ) {

  // Use cached list instead, 
  // to avoid overhead of the generation of this list.
  $transient_name = 'servicelocations-' . $field['key'];
  if (get_transient($transient_name)) {
    return get_transient($transient_name);
  }

  // Switch to main site
  switch_to_blog(1);
  
  // reset choices
  $field['choices'] = array();

  // Get locations
  $locations_args = array(
    'post_type' => 'locations',
    'posts_per_page' => -1,
    'post_status' => 'publish',
    'orderby'     => 'title',
    'order'       => 'ASC'
  );
  $locations_loop = null;
  $locations_loop = new WP_Query($locations_args);
  if ( $locations_loop->have_posts() ):
    while($locations_loop->have_posts()) : $locations_loop->the_post();
      $value = get_the_ID();
      $label = get_the_title();

      // append to choices
      $field['choices'][ $value ] = $label;
    endwhile;
  endif;

  if (ms_is_switched()) {
    restore_current_blog();
  }

  // Store results for next time
  set_transient($transient_name, $field, 60 * 12 * MINUTE_IN_SECONDS); // remember for 12 hours

  // return the field
  return $field;
}

add_filter('acf/load_field/name=location_select', 'acf_load_servicelocation_field_choices');
add_filter('acf/load_field/name=ministry_location_select', 'acf_load_servicelocation_field_choices');
add_filter('acf/load_field/name=locations_checkbox', 'acf_load_servicelocation_field_choices');
add_filter('acf/load_field/name=location_site', 'acf_load_servicelocation_field_choices');



/*
 * ACF Multisite - Make a Page Link Select Box for all Sites
 *
 * Ref: https://www.advancedcustomfields.com/resources/dynamically-populate-a-select-fields-choices/
 */
function acf_load_allsiteslinks_field_choices( $field ) {

  // Use cached list instead, 
  // to avoid overhead of the generation of this list.
  if (get_transient('allsitelinks')) {
    return get_transient('allsitelinks');
  }

  // reset choices
  $field['choices'] = array();

  // Get all Sites Array
  $all_sites = get_sites( array(
    'orderby'=>'domain'
  ));

  // Loop through sites.
  foreach ( $all_sites as $site ):
    switch_to_blog( $site->blog_id );
      wp_reset_postdata();

    $site_id = $site->blog_id;
    $site_name = get_bloginfo('name');
    $site_url = get_home_url();
    $site_domain = str_replace(['http://','https://'], '', $site_url);

    // pages
    $page_args = array(
      'post_type' => 'page',
      'posts_per_page' => -1,
      'post_status' => 'publish',
      'orderby'     => 'title',
      'order'       => 'ASC'
    );
    $page_loop = null;
    $page_loop = new WP_Query($page_args);
    if ( $page_loop->have_posts() ):
      while($page_loop->have_posts()) : $page_loop->the_post();
        $value = get_the_permalink();
        $label = $site_domain . ' > PAGE > ' . get_the_title();

        // append to choices
        $field['choices'][ $value ] = $label;
      endwhile;
      wp_reset_postdata();
    endif;


    // services
    $services_args = array(
      'post_type' => 'services',
      'posts_per_page' => -1,
      'post_status' => 'publish',
      'orderby'     => 'title',
      'order'       => 'ASC'
    );
    $services_loop = null;
    $services_loop = new WP_Query($services_args);
    if ( $services_loop->have_posts() ):
      while($services_loop->have_posts()) : $services_loop->the_post();
        $value = get_the_permalink();
        $label = $site_domain . ' > SERVICE > ' . get_the_title();

        // append to choices
        $field['choices'][ $value ] = $label;
      endwhile;
      wp_reset_postdata();
    endif;


    // events
    $events_args = array(
      'post_type' => 'events',
      'posts_per_page' => -1,
      'post_status' => 'publish',
      'orderby'     => 'title',
      'order'       => 'ASC'
    );
    $events_loop = null;
    $events_loop = new WP_Query($events_args);
    if ( $events_loop->have_posts() ):
      while($events_loop->have_posts()) : $events_loop->the_post();
        $value = get_the_permalink();
        $label = $site_domain . ' > EVENT > ' . get_the_title();

        // append to choices
        $field['choices'][ $value ] = $label;
      endwhile;
      wp_reset_postdata();
    endif;

    // locations
    $location_args = array(
      'post_type' => 'ministrylocations',
      'posts_per_page' => -1,
      'post_status' => 'publish',
      'orderby'     => 'title',
      'order'       => 'ASC'
    );
    $location_loop = null;
    $location_loop = new WP_Query($location_args);
    if ( $location_loop->have_posts() ):
      while($location_loop->have_posts()) : $location_loop->the_post();
        $value = get_the_permalink();
        $label = $site_domain . ' > LOCATION > ' . get_the_title();

        // append to choices
        $field['choices'][ $value ] = $label;
      endwhile;
      wp_reset_postdata();
    endif;

    // posts
    $post_args = array(
      'post_type' => 'post',
      'posts_per_page' => -1,
      'post_status' => 'publish',
      'orderby'     => 'title',
      'order'       => 'ASC'
    );
    $post_loop = null;
    $post_loop = new WP_Query($post_args);
    if ( $post_loop->have_posts() ):
      while($post_loop->have_posts()) : $post_loop->the_post();
        $value = get_the_permalink();
        $label = $site_domain . ' > POST > ' . get_the_title();

        // append to choices
        $field['choices'][ $value ] = $label;
      endwhile;
      wp_reset_postdata();
    endif;

    restore_current_blog();
  endforeach;

  // Store results for next time
  set_transient('allsitelinks', $field, 60 * MINUTE_IN_SECONDS); // remember for 1 hour

  // return the field
  return $field;
  
}
add_filter('acf/load_field/name=allsiteslinks_select', 'acf_load_allsiteslinks_field_choices');


/* Select for Give Forms */
function acf_load_mainsitegiveform_field_choices( $field ) {

  // reset choices
  $field['choices'] = array();
  $field['choices']['none'] = "None";

  // Loop through sites.
  // switch_to_blog( 1 );
    // wp_reset_postdata();

    $give_args = array( 
      'post_type' => 'give_forms', 
      'posts_per_page' => -1,
      'post_status' => 'publish',
      'orderby'    => 'post_title',
      'order'       => 'ASC',
    );
    $give_loop = new WP_Query( $give_args );
    if( $give_loop->have_posts() ):
      while ( $give_loop->have_posts() ) : $give_loop->the_post();
        $value = get_the_ID();
        $label = get_the_title();
        $field['choices'][ $value ] = $label;
      endwhile;
    endif;

  // restore_current_blog();
  wp_reset_postdata();

  // return the field
  return $field;
  
}
add_filter('acf/load_field/name=mainsitegiveforms_select', 'acf_load_mainsitegiveform_field_choices');



/**
 * Allow Duplication of posts in Admin
 *
 * Note: must set the post types to allow duplication near the bottom of the funciton
 *
 * Note: does NOT work for hierarchical posts! see wp ref. 
 *
 * Function creates post duplicate as a draft and redirects then to the edit post screen
 * Ref: https://rudrastyh.com/wordpress/duplicate-post.html
 * WP Ref: https://developer.wordpress.org/reference/hooks/post_row_actions/
 */
function rd_duplicate_post_as_draft(){
  global $wpdb;
  if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
    wp_die('No post to duplicate has been supplied!');
  }

  /*
   * get the original post id
   */
  $post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
  /*
   * and all the original post data then
   */
  $post = get_post( $post_id );

  /*
   * if you don't want current user to be the new post author,
   * then change next couple of lines to this: $new_post_author = $post->post_author;
   */
  $current_user = wp_get_current_user();
  $new_post_author = $current_user->ID;

  /*
   * if post data exists, create the post duplicate
   */
  if (isset( $post ) && $post != null) {

    /*
     * new post data array
     */
    $args = array(
      'comment_status' => $post->comment_status,
      'ping_status'    => $post->ping_status,
      'post_author'    => $new_post_author,
      'post_content'   => $post->post_content,
      'post_excerpt'   => $post->post_excerpt,
      'post_name'      => $post->post_name,
      'post_parent'    => $post->post_parent,
      'post_password'  => $post->post_password,
      'post_status'    => 'draft',
      'post_title'     => $post->post_title,
      'post_type'      => $post->post_type,
      'to_ping'        => $post->to_ping,
      'menu_order'     => $post->menu_order
    );

    /*
     * insert the post by wp_insert_post() function
     */
    $new_post_id = wp_insert_post( $args );

    /*
     * get all current post terms ad set them to the new post draft
     */
    $taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
    foreach ($taxonomies as $taxonomy) {
      $post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
      wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
    }

    /*
     * duplicate all post meta just in two SQL queries
     */
    $post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
    if (count($post_meta_infos)!=0) {
      $sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
      foreach ($post_meta_infos as $meta_info) {
        $meta_key = $meta_info->meta_key;
        $meta_value = addslashes($meta_info->meta_value);
        $sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
      }
      $sql_query.= implode(" UNION ALL ", $sql_query_sel);
      $wpdb->query($sql_query);
    }


    /*
     * finally, redirect to the edit post screen for the new draft
     */
    wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
    exit;
  } else {
    wp_die('Post creation failed, could not find original post: ' . $post_id);
  }
}
add_action( 'admin_action_rd_duplicate_post_as_draft', 'rd_duplicate_post_as_draft' );

/*
 * Add the duplicate link to action list for post_row_actions
 */
function rd_duplicate_post_link( $actions, $post ) {
  // EDIT for post types to allow duplication
  if (in_array($post->post_type, array('page','events')) && current_user_can('edit_posts')) {
    $actions['duplicate'] = '<a href="admin.php?action=rd_duplicate_post_as_draft&amp;post=' . $post->ID . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
  }
  return $actions;
}

add_filter( 'page_row_actions', 'rd_duplicate_post_link', 9999, 2 );


/*
 * Admin NO Dashboard - land on Recipes
 *
 * Ref: http://www.wpbeginner.com/wp-themes/change-the-footer-in-your-wordpress-admin-panel/
 */
$new_home = "edit.php?post_type=page";

function new_dashboard_home($username, $user){
    if(array_key_exists('administrator', $user->caps)){
        wp_redirect(admin_url($new_home, 'http'), 301);
        exit;
    }
}
// add_action('wp_login', 'new_dashboard_home', 10, 2);
function dashboard_redirect(){
    wp_redirect(admin_url($new_home));
}
// add_action('load-index.php','dashboard_redirect');


/*
 * Admin Footer Credentials
 *
 * Ref: http://www.wpbeginner.com/wp-themes/change-the-footer-in-your-wordpress-admin-panel/
 */
function remove_footer_admin () {
  echo 'Design & Development by <a href="http://jumpsand.com" target="_blank">Jumpsand</a>';
}
add_filter('admin_footer_text', 'remove_footer_admin');



/**
 * REMOVE WP EMOJI!! Come on Wordpress
 *
 * Ref: https://smartcatdesign.net/articles/stop-loading-wp-emoji-release-min-js-wordpress-site/
 */
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

/**
 * REMOVE WP EMBED!! Come on Wordpress
 *
 * Ref: https://wordpress.stackexchange.com/questions/211701/what-does-wp-embed-min-js-do-in-wordpress-4-4
 */
function my_deregister_scripts(){
  wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );


/**
 * Google Maps API key.
 * This is under the Sunshine Mile project in the openform.co folder in Google Cloud Platform
 */
function my_acf_init()
{

  acf_update_setting('google_api_key', 'AIzaSyDuuKINsFxSVkkEhZflLBZNFnzCV9uW14k');
}

add_action('acf/init', 'my_acf_init');


/**
 * Force outgoing email address
 * Ref: https://postmansmtp.com/storedrv-submission-exceptionsendasdeniedexception-mapiexceptionsendasdenied/
 */
add_filter('wp_mail_from', 'change_contact_from_address', 999);
function change_contact_from_address() {
     return 'web@abcs.org';
}



/**
 *
 * Allow Iframes in Editor
 *
 * REF: https://tastyplacement.com/wordpress-stripping-iframe-elements-heres-the-fix
 */

function add_iframe($initArray) {
  $initArray['extended_valid_elements'] = "iframe[id|class|title|style|align|frameborder|height|longdesc|marginheight|marginwidth|name|scrolling|src|width]";
  return $initArray;
}

add_filter('tiny_mce_before_init', 'add_iframe');




/**
 *
 * REMOVE All Comments
 *
 * REF: https://www.wpbeginner.com/wp-tutorials/how-to-completely-disable-comments-in-wordpress/
 */

add_action('admin_init', function () {
    // Redirect any user trying to access comments page
    global $pagenow;
     
    if ($pagenow === 'edit-comments.php') {
        wp_safe_redirect(admin_url());
        exit;
    }
 
    // Remove comments metabox from dashboard
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
 
    // Disable support for comments and trackbacks in post types
    foreach (get_post_types() as $post_type) {
        if (post_type_supports($post_type, 'comments')) {
            remove_post_type_support($post_type, 'comments');
            remove_post_type_support($post_type, 'trackbacks');
        }
    }
});
 
// Close comments on the front-end
add_filter('comments_open', '__return_false', 20, 2);
add_filter('pings_open', '__return_false', 20, 2);
 
// Hide existing comments
add_filter('comments_array', '__return_empty_array', 10, 2);
 
// Remove comments page in menu
add_action('admin_menu', function () {
    remove_menu_page('edit-comments.php');
});
 
// Remove comments links from admin bar
add_action('init', function () {
    if (is_admin_bar_showing()) {
        remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
    }
});
