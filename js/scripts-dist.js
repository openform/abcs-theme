'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

(function ($) {
  var Scripts = {
    init: function init() {
      var self = this;
      this.bLazy();
      this.bindEvents();
      this.sticky();
      this.showHomeVideo();
      this.superNav();
      this.buildMap();
      this.adminSectionLinks();
      this.grabPDFLink();

      // Startup
      $(document).ready(function () {

        self.locationsFilter();

        setTimeout(function () {
          $('html').addClass('loaded');
          // self.stickyFooter();
          self.shouldCalcDistances();
          self.checkTranslate();
        }, 400);
      });
      // this.recipeIngredients();

      this.queryParams = function (a) {
        if (a == "") return {};
        var b = {};
        for (var i = 0; i < a.length; ++i) {
          var p = a[i].split('=', 2);
          if (p.length == 1) b[p[0]] = "";else b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
        }
        return b;
      }(window.location.search.substr(1).split('&'));
    },
    bLazy: function bLazy() {
      // Lazy Load images
      // Initialize on .b-lazy class
      document.addEventListener('DOMContentLoaded', function () {
        var bLazy = new Blazy({});
      }, false);
    },
    bindEvents: function bindEvents() {
      var self = this;
      // show mobile nav
      $(document).on("click", ".navbar .btn-menu", function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        $('html.nav-visible').find('.nav-collapse').hide();
        $(".navbar").find('.nav-collapse').show();
        $(".navbar").toggleClass('nav-visible');
        $('html').toggleClass('nav-visible');
      });

      // always close mobile nav
      $(document).on("click", ".nav-visible .nav-top li a", function () {
        self._close_nav();
      });

      // hero vertical height
      $(document).ready(function () {
        self.resizeDiv();
      });
      window.onresize = function () {
        self.resizeDiv();
      };

      $(document).bind('gform_post_render', function () {
        self.resizeDiv();
      });

      // search click
      $(document).on("click", ".nav-top .search-form .glyphicon-search", function () {
        $(".nav-top .search-form").submit();
      });
    },
    superNav: function superNav() {
      var self = this;
      // show mobile nav
      $(document).on("click", ".super-nav .super-link", function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        $('.super-nav').toggleClass('supernav-visible');
      });

      // always close mobile nav
      $(document).on("click", ".super-nav .content a", function () {
        $('.super-nav').toggleClass('supernav-visible');
      });
    },


    resizeDiv: function resizeDiv() {
      var mobileBreakpoint = 768;
      var vpw = $(window).width();
      var vph = $(window).height();
      var bottommargin = -250;
      // let navMargin = vph - 57;
      var heroheight = vph / 2;
      var heroheightTall = vph + bottommargin;
      var heroheightTallMobile = vph / 1.66;

      if (heroheight > 500) {
        heroheight = 500;
      } else if (heroheight < 350) {
        heroheight = 350;
      }
      if (heroheightTall < 470) {
        heroheightTall = 470;
      } else if (heroheightTall > 800) {
        heroheightTall = 800;
      }

      if (vpw < mobileBreakpoint) {
        $('.hero').css({ 'height': heroheight + 'px' });
        $('.hero-bg').css({ 'height': heroheight + 'px' });
        $('.hero.hero-tall').css({ 'height': heroheightTallMobile + 'px' });
        $('.hero.hero-tall .hero-bg').css({ 'height': heroheightTallMobile + 'px' });
        $('.hero.hero-text').css({ 'height': 'auto' });
        $('.fullwidth-image').css({ 'height': heroheight + 'px' });
        $('.fullwidth-image-bg').css({ 'height': heroheight + 'px' });
        $('#top-nav').addClass('mobile-nav');
      } else {
        $('.hero').css({ 'height': heroheight + 'px' });
        $('.hero-bg').css({ 'height': heroheight + 'px' });
        $('.hero.hero-tall').css({ 'height': heroheightTall + 'px' });
        $('.hero.hero-tall .hero-bg').css({ 'height': heroheightTall + 'px' });
        $('.hero.hero-text').css({ 'height': 'auto' });
        $('.fullwidth-image').css({ 'height': heroheight + 'px' });
        $('.fullwidth-image-bg').css({ 'height': heroheight + 'px' });
        $('#top-nav').removeClass('mobile-nav');
      }

      // this.stickyFooter();
    },

    stickyFooter: function stickyFooter() {
      var vph = $(window).height();
      var footerHeight = $('#site-footer').outerHeight();
      var contentHeight = $('#content').outerHeight();
      if (contentHeight + footerHeight < vph) {
        $('body').addClass('sticky-foot');
        $('body.sticky-foot').css({ 'padding-bottom': footerHeight });
      } else {
        $('body.sticky-foot').css({ 'padding-bottom': 0 }).removeClass('sticky-foot');;
      }
    },

    sticky: function sticky() {
      // Sticky nav
      document.addEventListener('DOMContentLoaded', function () {
        var topnav = $('#content-marker').waypoint(function (direction) {
          console.log('content hit window top', direction);
          if (direction === 'up') {
            $('.top-nav').removeClass('shrink');
          }
          if (direction === 'down') {
            $('.top-nav').addClass('shrink');
          }
        }, {
          offset: '60px'
        });
      });
    },

    showHomeVideo: function showHomeVideo() {
      var vid = $("#bgvid");
      var ua = navigator.userAgent;
      var iphone = /iPhone/i.test(ua);
      var android = /Android/i.test(ua);

      if (!iphone && !android) {
        vid.on("canplay", function () {
          vid[0].play();
          vid.removeClass('video-wait');
        });
      }

      if (iphone) {
        $('#bgvid').addClass('iphone');
        $(document).on("click", ".btn-play", function () {
          $('body').addClass('showVideo');
          $('#bgvid').removeClass('video-wait');

          // mobile support that requires the video to be visible
          if (vid[0].paused) {
            vid[0].play();

            setTimeout(function () {
              $('#bgvid').prop('muted', false);
            }, 200);

            vid.bind('play ended', function () {
              $('body').removeClass('showVideo');
              $('#bgvid').prop('muted', true);
              $("#bgvid").addClass('video-wait');
            });
          }
        });
      } else if (android) {
        // android
        $(document).on("click", ".btn-play", function () {
          // mobile support that requires the video to be visible
          if (vid[0].paused) {
            vid[0].play();
          }

          // give it a little pause
          setTimeout(function () {
            vid.removeClass('video-wait');
            $('body').addClass('showVideo');
            $('#bgvid').prop('muted', false).removeClass('video-wait');
          }, 200);

          vid.bind('play ended', function () {
            $('body').removeClass('showVideo');
            $('#bgvid').prop('muted', true);
            $("#bgvid").addClass('video-wait');
          });
        });
        $(document).on("click", ".hide-video", function () {
          $('body').removeClass('showVideo');
          $('#bgvid').prop('muted', true);
        });
      } else {
        // web
        $(document).on("click", ".btn-play", function () {
          $('body').addClass('showVideo');
          $('#bgvid').prop('muted', false).removeClass('video-wait');
        });
        $(document).on("click", ".hide-video", function () {
          $('body').removeClass('showVideo');
          $('#bgvid').prop('muted', true);
        });
      }
    },
    adminSectionLinks: function adminSectionLinks() {
      $('.admin-viewer main > div > section[id], .admin-viewer main > div > div[id]').each(function (idx) {
        var id = $(this).attr('id');
        var newdiv = document.createElement('a');
        newdiv.href += window.location.href + '#' + id;
        newdiv.className += 'idshow';
        newdiv.innerHTML += '#' + id;
        this.append(newdiv);
      });

      var hash = window.location.hash;
      if (hash) {
        $(document).ready(function () {
          var hh = $("header").height();
          $('html, body').stop().animate({
            scrollTop: $(hash).offset().top - hh
          }, 800, 'swing');
        });
      }
    },
    grabPDFLink: function grabPDFLink() {
      $(document).on('click', 'a[href$=".pdf"]', function (ev) {
        ev.preventDefault();
        var pdfurl = $(ev.target).attr('href');
        $('form #pdf_download_url_value').val(pdfurl);
        $('form#force_pdf_open').submit();
      });
    },
    checkTranslate: function checkTranslate() {
      if ($('#trp-floater-ls').length) {
        $('body').addClass('translating');
      }
    },
    shouldCalcDistances: function shouldCalcDistances() {
      var self = this;
      if (!$('.calc-distance').length) {
        return;
      }

      var userLocation = this.getLocation();

      $('.calc-distance').each(function () {
        var target = $(this)[0].dataset;
        var distance = self.calcDistance(userLocation.latitude, userLocation.longitude, target.latitude, target.longitude);
        $(this).attr('data-distance', Math.round(distance));
      });
    },
    calcDistance: function calcDistance(lat1, lon1, lat2, lon2, unit) {
      var radlat1 = Math.PI * lat1 / 180;
      var radlat2 = Math.PI * lat2 / 180;
      var theta = lon1 - lon2;
      var radtheta = Math.PI * theta / 180;
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
        dist = 1;
      }
      dist = Math.acos(dist);
      dist = dist * 180 / Math.PI;
      dist = dist * 60 * 1.1515;
      if (unit == "K") {
        dist = dist * 1.609344;
      }
      if (unit == "N") {
        dist = dist * 0.8684;
      }
      return dist;
    },
    getLocation: function getLocation() {
      var self = this;

      var userCoord = {
        'latitude': localStorage.getItem('startLat'),
        'longitude': localStorage.getItem('startLon')
      };

      var geoSuccess = function geoSuccess(position) {
        var startPos = position;
        localStorage.setItem('startLat', startPos.coords.latitude);
        localStorage.setItem('startLon', startPos.coords.longitude);

        return startPos.coords;
      };

      var geoError = function geoError(error) {
        $.getJSON('https://ipinfo.io/geo', function (response) {
          var loc = response.loc.split(',');
          var coords = {
            latitude: loc[0],
            longitude: loc[1]
          };

          localStorage.setItem('startLat', coords.latitude);
          localStorage.setItem('startLon', coords.longitude);

          return coords;
        }, function (error) {
          switch (error.code) {
            case error.TIMEOUT:
              return false;
              break;
          }
        });
      };

      if (typeof userCoord.latitude === 'undefined' || userCoord.latitude === null) {
        userCoord = navigator.geolocation.getCurrentPosition(geoSuccess, geoError);
      }

      return userCoord;
    },
    _updateMapPosition: function _updateMapPosition(zoom) {
      var userCoord = this.getLocation();
      var mapZoom = 9;

      if (userCoord !== undefined) {
        if (zoom !== undefined && zoom !== 0) {
          mapZoom = zoom;
        }
        this.map.jumpTo({ 'center': [userCoord.longitude, userCoord.latitude], 'zoom': mapZoom });
      }
    },
    _getActiveLocations: function _getActiveLocations() {
      return $('#map location');
    },
    buildMap: function buildMap() {
      var self = this;
      var mapCenter = [-112, 33.292394];
      var mapZoom = 5.35;
      var focus = false;

      if ($('#map').length > 0) {
        var features = [];

        var _loop = function _loop() {
          var locD = $('#map location')[i].dataset;

          if (locD.longitude == '' || locD.latitude == '') {
            // Try to get lat/long from Mapbox Search Api
            var newCoords = async function newCoords() {
              var newCoords = await self._getLatLong(locD.address);
              locD.longitude = newCoords.longitude;
              locD.latitude = newCoords.latitude;
            };
          }

          if (locD.focus) {
            mapCenter = [locD.longitude, locD.latitude];
            mapZoom = 14;
            focus = true;

            if (parseInt(locD.focus) > 0 && parseInt(locD.focus) < 20) {
              mapZoom = locD.focus;
            }
          }

          var description = '<h4>' + locD.name + '</h4><p class="text-sans">' + locD.address + '</p>';

          if (locD.url !== null && _typeof(locD.url) !== undefined && locD.url !== '') {
            description = '<h4>' + locD.name + '</h4><p class="text-sans">' + locD.address + '<br/><a href=\"' + locD.url + '\" title=\"View location\" class=\"link-tiny brand-textcolor\">View details</a></p>';
          }

          features.push({
            "type": "Feature",
            "geometry": {
              "type": "Point",
              "coordinates": [locD.longitude, locD.latitude]
            },
            "properties": {
              "title": locD.name,
              "icon": "marker",
              "description": description,
              "marker-color": "#F9423A",
              "services": locD.services
            }
          });
        };

        for (var i = 0; i < self._getActiveLocations().length; i++) {
          _loop();
        }

        if ($('#map')[0].dataset.zoom) {
          var mapData = $('#map')[0].dataset;
          mapCenter = [mapData.lng, mapData.lat];
          mapZoom = mapData.zoom;

          if ($(window).width() < 768) {
            mapZoom = mapZoom * .9;
          }
        }

        mapboxgl.accessToken = self._getMapboxAccessToken();

        this.map = new mapboxgl.Map({
          container: 'map',
          style: 'mapbox://styles/abaumer/cjlzl840a6jm62rmqfbrcvyfv',
          center: mapCenter,
          zoom: mapZoom
        });

        var geojson = {
          id: 'places',
          type: 'FeatureCollection',
          features: features
        };

        geojson.features.forEach(function (marker) {
          var el = document.createElement('i');
          el.className = 'fa fa-map-marker marker calc-distance ' + (marker.properties.services !== undefined ? marker.properties.services : '');
          el.setAttribute('data-latitude', marker.geometry.coordinates[1]);
          el.setAttribute('data-longitude', marker.geometry.coordinates[0]);

          new mapboxgl.Marker(el).setLngLat(marker.geometry.coordinates).setPopup(new mapboxgl.Popup({ offset: 25 }) // add popups
          .setHTML(marker.properties.description)).addTo(self.map);
        });

        if (focus !== true) {
          this._updateMapPosition();
        }
      }
    },
    _getMapboxAccessToken: function _getMapboxAccessToken() {
      return 'pk.eyJ1IjoiYWJhdW1lciIsImEiOiJjamx6bDdmam0wcWdkM3Zud3l4M3pqNDVrIn0.keE7F4-2yE4JDdtxouGgrg';
    },
    _getLatLong: async function _getLatLong(address) {
      var self = this;
      var mbUrl = "https://api.mapbox.com/geocoding/v5/mapbox.places/" + encodeURIComponent(address) + ".json?limit=1&access_token=" + self._getMapboxAccessToken();
      await $.getJSON(mbUrl, function (response) {
        console.log("gecoding forward lookup response: ", response);
        var loc = response.features[0].center;
        var coords = {
          latitude: loc[0],
          longitude: loc[1]
        };

        return coords;
      }, function (error) {
        switch (error.code) {
          case error.TIMEOUT:
            return false;
            break;
        }
      });
    },
    _updateQueryStringParameter: function _updateQueryStringParameter(key, value) {
      var uri = window.location.href;
      var re = new RegExp("([?&])" + key + "=.*?(&|#|$)", "i");
      if (value === undefined) {
        if (uri.match(re)) {
          return uri.replace(re, '$1$2');
        } else {
          return uri;
        }
      } else {
        if (uri.match(re)) {
          return uri.replace(re, '$1' + key + "=" + value + '$2');
        } else {
          var hash = '';
          if (uri.indexOf('#') !== -1) {
            hash = uri.replace(/.*#/, '#');
            uri = uri.replace(/#.*/, '');
          }
          var separator = uri.indexOf('?') !== -1 ? "&" : "?";
          return uri + separator + key + "=" + value + hash;
        }
      }
    },
    _filterServices: function _filterServices(newService) {
      if (newService === 'all' || newService == undefined) {
        $('.list-locations > li, .mapboxgl-canvas-container > .marker').removeClass('showservice').addClass('allservices');
      } else {
        $('.list-locations > li, .mapboxgl-canvas-container > .marker').removeClass('allservices showservice');

        $('.list-locations').find('.ms-' + newService).addClass('showservice');
        $('.mapboxgl-canvas-container').find('.ms-' + newService).addClass('showservice');
      }
    },
    _filterMinistries: function _filterMinistries(newMinistry) {
      if (newMinistry === 'all' || newMinistry == undefined) {
        $('.list-locations > li, .mapboxgl-canvas-container > .marker').removeClass('showministry').addClass('allministries');
      } else {
        $('.list-locations > li, .mapboxgl-canvas-container > .marker').removeClass('allministries showministry');

        $('.list-locations').find('.mn-' + newMinistry).addClass('showministry');
        $('.mapboxgl-canvas-container').find('.mn-' + newMinistry).addClass('showministry');
      }
    },
    locationsFilter: function locationsFilter() {
      var self = this;

      if (!$('.locations-map').length > 0) {
        return;
      }

      var activeService = this.queryParams['ms'];
      this._filterServices(activeService);
      if (activeService == undefined) activeService = 'all';
      $('#serviceFilter').val(activeService);

      var activeMinistry = this.queryParams['mn'];
      this._filterMinistries(activeMinistry);
      if (activeMinistry == undefined) activeMinistry = 'all';
      $('#ministryFilter').val(activeMinistry);

      // get hash
      // if hash, set filter - adjust select box, show only filtered locations

      $('#serviceFilter').on('change', function () {
        var newService = this.value;
        self._filterServices(newService);

        var newUrl = self._updateQueryStringParameter('ms', newService);
        // newUrl = self._updateQueryStringParameter('mn', 'all');
        window.history.replaceState({ path: newUrl }, '', newUrl);
      });

      $('#ministryFilter').on('change', function () {
        var newMinistry = this.value;
        self._filterMinistries(newMinistry);

        var newUrl = self._updateQueryStringParameter('mn', newMinistry);
        // newUrl = self._updateQueryStringParameter('ms', 'all');
        window.history.replaceState({ path: newUrl }, '', newUrl);
      });

      $('#geoFilter').on('change', function () {
        var newDistance = this.value;
        console.log('new distance: ', newDistance);
        if (newDistance === 'all') {
          $('.list-locations > li').removeClass('distance-far');
          $('.mapboxgl-canvas-container > .marker').removeClass('distance-far');
        } else {
          $('.list-locations > li, .mapboxgl-canvas-container > .marker').removeClass('distance-far').filter(function (idx, el) {
            return parseInt($(el).attr('data-distance')) >= parseInt(newDistance);
          }).addClass('distance-far');

          if (newDistance > 10 && newDistance < 50) self._updateMapPosition(8);else if (newDistance > 50) self._updateMapPosition(6);else self._updateMapPosition(9);
        }
      });
    },
    _close_nav: function _close_nav() {
      $('.navbar').removeClass('nav-visible');
      $('html').removeClass('nav-visible');
    },
    _open_nav: function _open_nav() {
      $(".navbar").find('.nav-collapse').show();
      $(".navbar").addClass('nav-visible');
      $('html').addClass('nav-visible');
    },


    videoFrameWrapper: function videoFrameWrapper() {
      $(".container").find("iframe").wrap("<div class='video-wrapper'/>");
    }
  };

  Scripts.init();
})(jQuery);
//# sourceMappingURL=scripts-dist.js.map
